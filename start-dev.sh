#!/bin/bash

set -ex

echo "Start Backend"
gnome-terminal -- bash -c "cd ./backend && npm run start-dev; exec bash;"

echo "Start Frontend"
gnome-terminal -- bash -c "cd ./frontend && npm run start-dev; exec bash;"


if [[ "$*" == *"init"* ]]
then
    echo "INIT"
    gnome-terminal -- bash -c "cd ./backend && npm run initDatabase-dev; exec bash;"
fi