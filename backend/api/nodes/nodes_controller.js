
"use strict";

import { format } from 'date-fns';

import FileSystemTools from "../../tools/fileSystem_tools.js";
import NeurochainTools from "../../tools/neurochain_tools.js";
import ShellTools from "../../tools/shell_tools.js";
import SshTools from "../../tools/ssh_tools.js";

export default class NodesController {
    static async list(req, res, next) {
        const rows = NeurochainTools.getNodes({ networkId: req.params.networkId });
        return res.status(200).json({ rows: rows });
    }
    
    static async read(req, res, next) {
        const row = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        return res.status(200).json({ row: row });
    }

    static async getReady(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getReady({ hostname: node.hostname });
        
        return res.status(response.status).json({ ready: response.data === "{ok: 1}" });
    }

    static async getPeers(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getPeers({ hostname: node.hostname });
        
        return res.status(response.status).json({ rows: response.data.peers });
    }

    static async getStatus(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getStatus({ hostname: node.hostname });
        
        return res.status(response.status).json({ status: response.data });
    }

    static async getDockers(req, res, next) {
        let response = { status: 400, data: "" };

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const feedback = await SshTools.send({ 
            hostname: node.hostname,
            username: node.username, 
            sshKeyPath: NeurochainTools.getSshKeyPath({ networkId: req.params.networkId }),
            command: "docker ps",
        });
        if(feedback && feedback.code === 0) {
            response.status = 200
            response.data = feedback.stdout.split("\n");
        } else {
            response.data = feedback?.stderr;
        }
        
        return res.status(response.status).json({ rows: response.data });
    }

    static async getLogs(req, res, next) {
        const elementsPerPage = req.query.elementsPerPage ? req.query.elementsPerPage : 20;

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await SshTools.executeOnDockerContainer({ 
            hostname: node.hostname,
            username: node.username, 
            sshKeyPath: NeurochainTools.getSshKeyPath({ networkId: req.params.networkId }),
            action: "logs",
            containerId: "core",
            options: { elementsPerPage: elementsPerPage }
        });
        
        return res.status(200).json({ rows: response.data });
    }

    static async getWallet(req, res, next) {
        const outputKeysPath = `${process.cwd()}/inputs/networks/${req.params.networkId}/${req.params.nodeId}/keys`;

        if( !FileSystemTools.doesExist({ inputPath: outputKeysPath }) ) {
            FileSystemTools.createDirectory({ inputPath: outputKeysPath });
        }

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const sshKeyPath = NeurochainTools.getSshKeyPath({ networkId: req.params.networkId });
        const remotePath = `~/conf`;
        if( !FileSystemTools.doesExist({ inputPath: outputKeysPath + `/key.priv` }) ) {
            await SshTools.copyFrom({
                hostname: node.hostname,
                username: node.username, 
                sshKeyPath: sshKeyPath,
                remotePath: remotePath + `/key.priv`,
                localPath: outputKeysPath + `/key.priv`
            });
        }
        if( !FileSystemTools.doesExist({ inputPath: outputKeysPath + `/key.pub` }) ) { 
            await SshTools.copyFrom({
                hostname: node.hostname,
                username: node.username, 
                sshKeyPath: sshKeyPath,
                remotePath: remotePath + `/key.pub`,
                localPath: outputKeysPath + `/key.pub`
            });
        }
        
        const keys = await ShellTools.execute({ command: `${process.cwd()}/tools/ECDSA -decipher -privateKeyPath "${outputKeysPath + `/key.priv`}" -publicKeyPath "${outputKeysPath + `/key.pub`}"` });

        return res.status(200).json({ row: JSON.parse(keys) });
    }

    static async getBotJson(req, res, next) {
        let output = {};

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const sshKeyPath = NeurochainTools.getSshKeyPath({ networkId: req.params.networkId });
        const response = await SshTools.send({ hostname: node.hostname,
            username: node.username, 
            sshKeyPath: sshKeyPath,
            command: `cat ~/conf/bot.json`
        });

        if( response.code === 0 ) {
            output = JSON.parse(response.stdout);
        }

        return res.status(200).json({ row: output });
    }

    static async backupMongo(req, res, next) {
        let outputName = `mongo_${format(new Date(), 'yyyyMMdd')}`;

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const sshKeyPath = NeurochainTools.getSshKeyPath({ networkId: req.params.networkId });
        await SshTools.send({ hostname: node.hostname,
            username: node.username, 
            sshKeyPath: sshKeyPath,
            command: `sudo cp -r ~/mongo ~/${outputName}`
        });
        if( response.code != 0 ) {
            outputName = response.stderr;
        }

        return res.status(200).json({ row: outputName });
    }

    static async getBalance(req, res, next) {
        let balance = 0;

        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getBalance({ hostname: node.hostname, publicKey: req.params.walletId });

        if( response.status === 200 ) {
            balance = parseFloat(response.data.value)
        }
        
        return res.status(response.status).json({ balance: balance });
    }
}