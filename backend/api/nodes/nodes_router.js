
"use strict";

import { Router } from "express";

import NodesController from "./nodes_controller.js";

const router = new Router();

router
    .route("/networks/:networkId/nodes")
    .get( (req, res, next ) => NodesController.list(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId")
    .get( (req, res, next ) => NodesController.read(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/ready")
    .get( (req, res, next ) => NodesController.getReady(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/peers")
    .get( (req, res, next ) => NodesController.getPeers(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/status")
    .get( (req, res, next ) => NodesController.getStatus(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/dockers")
    .get( (req, res, next ) => NodesController.getDockers(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/core/logs")
    .get( (req, res, next ) => NodesController.getLogs(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/wallet")
    .get( (req, res, next ) => NodesController.getWallet(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/botJson")
    .get( (req, res, next ) => NodesController.getBotJson(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/backups/mongo")
    .get( (req, res, next ) => NodesController.backupMongo(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/wallet/:walletId/balance")
    .get( (req, res, next ) => NodesController.getBalance(req, res, next) )
;

export default router;