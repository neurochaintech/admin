"use strict";

import fs from "fs";

import FileSystemTools from "../../tools/fileSystem_tools.js";
import NeurochainTools from "../../tools/neurochain_tools.js";

export default class NetworksController {
    static async list(req, res, next) {
        let rows = [];
        
        rows = NeurochainTools.getNetworks();

        return res.status(200).json({ rows: rows });
    }

    static async create(req, res, next) {
        let output = { status: 400, data: {} };

        /*
        req.file = {
            fieldname: 'pemFile',
            originalname: 'sshKey.pem',
            encoding: '7bit',
            mimetype: 'application/x-x509-ca-cert',
            destination: 'uploads/',
            filename: '3eab1e2173350094501c3a398c1a5a69',
            path: 'uploads/3eab1e2173350094501c3a398c1a5a69',
            size: 1700
        }
        */
        // TODO: check no space in name nor special char => alpha num and "-_"
        if( req.body.name
            && req.body.config
            && req.file
            ) {
                const outputDirPath = `${process.cwd()}/inputs/networks/${req.body.name}`;
                FileSystemTools.createDirectory({ inputPath: outputDirPath });
                FileSystemTools.saveJsonToFile({ inputPath: `${outputDirPath}/config.json`, jsonData: JSON.parse(req.body.config) });

                const uploadPath = `${process.cwd()}/${req.file.destination}${req.file.filename}`;
                const outputSshPath = `${outputDirPath}/sshKey.pem`;
                FileSystemTools.copyFile({ inputPath: uploadPath, outputPath: outputSshPath });
                FileSystemTools.setFilePermissions({ inputPath: outputSshPath, permissions: fs.constants.S_IRUSR });
                FileSystemTools.deleteFile({ inputPath: uploadPath });

                output = { status: 201, data: {} }
        } else {
            output.data.message = "CONTROLLER - Missing inputs.";
        }                

        return res.status(output.status).json({ row: output.data });
    }

    static async getConfig(req, res, next) {
        let output = { status: 400, data: {} };

        try {
                const dirPath = `${process.cwd()}/inputs/networks/${req.params.networkId}`;
                const config = FileSystemTools.readJson({ inputPath: `${dirPath}/config.json`});
                
                output = { status: 200, data: config }
        } catch(error) {
            output.data.message = "CONTROLLER - Missing inputs.";
        }                

        return res.status(output.status).json({ rows: output.data });
    }

    static async updateConfig(req, res, next) {
        let output = { status: 400, data: {} };

        if( req.body.config ) {
                const dirPath = `${process.cwd()}/inputs/networks/${req.params.networkId}`;
                FileSystemTools.saveJsonToFile({ inputPath: `${dirPath}/config.json`, jsonData: JSON.parse(req.body.config) });

                output = { status: 200, data: {} }
        } else {
            output.data.message = "CONTROLLER - Missing inputs.";
        }                

        return res.status(output.status).json({ row: output.data });
    }
}