"use strict";

import { Router } from "express";
import multer from 'multer';
const upload = multer({ dest: 'uploads/' })

import NetworksController from "./networks_controller.js";

const router = new Router();

router
    .route("/networks")
    .get( (req, res, next ) => NetworksController.list(req, res, next) )
    .post( upload.single("pemFile"), (req, res, next ) => NetworksController.create(req, res, next) )
;

router
    .route("/networks/:networkId/config")
    .get( (req, res, next ) => NetworksController.getConfig(req, res, next) )
    .put( (req, res, next ) => NetworksController.updateConfig(req, res, next) )
;


export default router;