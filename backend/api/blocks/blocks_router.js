
"use strict";

import { Router } from "express";

import BlocksController from "./blocks_controller.js";

const router = new Router();

router
    .route("/networks/:networkId/nodes/:nodeId/blocks/count")
    .get( (req, res, next ) => BlocksController.count(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/blocks/latest/:elementsPerPage")
    .get( (req, res, next ) => BlocksController.listLatests(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/blocks/:blockId")
    .get( (req, res, next ) => BlocksController.readById(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/blocks/index/:index")
    .get( (req, res, next ) => BlocksController.readByIndex(req, res, next) )
;

export default router;