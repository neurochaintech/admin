
"use strict";

import NeurochainTools from "../../tools/neurochain_tools.js";

export default class BlocksController {
    static async count(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getBlocksCount({ hostname: node.hostname });
        
        return res.status(response.status).json({ count: response.data });
    }

    static async listLatests(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getLatestsBlocks({ hostname: node.hostname, elementsPerPage: req.params.elementsPerPage });
        
        return res.status(response.status).json({ rows: response.data.block });
    }

    static async readById(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getBlockById({ hostname: node.hostname, id: req.params.blockId });
        
        return res.status(response.status).json({ row: response.data });
    }

    static async readByIndex(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getBlockByIndex({ hostname: node.hostname, index: req.params.index });
        
        return res.status(response.status).json({ row: response.data });
    }
}