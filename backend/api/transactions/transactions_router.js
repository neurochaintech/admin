
"use strict";

import { Router } from "express";

import TransactionsController from "./transactions_controller.js";

const router = new Router();

// router
//     .route("/networks/:networkId/nodes/:nodeId/transactions/public-key/:publicKey")
//     .get( (req, res, next ) => TransactionsController.listTransactions(req, res, next) )
// ;

router
    .route("/networks/:networkId/nodes/:nodeId/transactions/authors/:publicKey")
    .get( (req, res, next ) => TransactionsController.listTransactions(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/transactions/recipients/:publicKey")
    .get( (req, res, next ) => TransactionsController.listTransactions(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/transactions/count")
    .get( (req, res, next ) => TransactionsController.count(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/transactions/:transactionId")
    .get( (req, res, next ) => TransactionsController.read(req, res, next) )
;

router
    .route("/networks/:networkId/nodes/:nodeId/transactions")
    .post( (req, res, next ) => TransactionsController.sendTransaction(req, res, next) )
;


export default router;