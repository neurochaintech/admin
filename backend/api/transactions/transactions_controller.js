
"use strict";

import NeurochainTools from "../../tools/neurochain_tools.js";

export default class TransactionsController {
    static async sendTransaction(req, res, next) {
        let output = { status: 400, data: {} };

        // transaction value must be > 0
        if( req.body.authorPublicKey
            && req.body.authorPrivateKey
            && req.body.recipientPublicKey
            && typeof req.body.value !== "undefined" && req.body.value > 0
            && req.body.data
            ) {
            const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
            output = await NeurochainTools.sendTransaction({ 
                hostname: node.hostname, 
                authorPublicKey: req.body.authorPublicKey, 
                authorPrivateKey: req.body.authorPrivateKey, 
                recipientPublicKey: req.body.recipientPublicKey,
                value: req.body.value, 
                data: req.body.data, 
                fee: req.body.fee ? req.body.fee : 0
            });
        } else {
            output.data.message = "CONTROLLER - Missing elements, required: authorPublicKey, authorPrivateKey, recipientPublicKey, value, data, fee.";
        }                

        return res.status(output.status).json({ row: output.data });
    }

    static async count(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getTransactionsCount({ hostname: node.hostname });
        
        return res.status(response.status).json({ count: response.data });
    }

    static async listTransactions(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });

        const page = req.query.page ? req.query.page : 0;
        const elementsPerPage = req.query.elementsPerPage ? req.query.elementsPerPage : 20;
        const params = { 
            hostname: node.hostname, 
            publicKey: req.params.publicKey,
            page: page,
            elementsPerPage: elementsPerPage
        };

        let response = { status: 400, data: {} };
        // if( req.originalUrl.includes("public-key") ) {
        //     response = await NeurochainTools.getTransactions(params);
        // }
        if( req.originalUrl.includes("authors") ) {
            response = await NeurochainTools.getTransactionsAuthor(params);
        }
        if( req.originalUrl.includes("recipients") ) {
            response = await NeurochainTools.getTransactionsRecipient(params);
        }
        
        return res.status(response.status).json({ rows: response.data.transactions });
    }

    static async read(req, res, next) {
        const node = NeurochainTools.getNode({ networkId: req.params.networkId, nodeId: req.params.nodeId });
        const response = await NeurochainTools.getTransaction({ hostname: node.hostname, id: req.params.transactionId });
        
        return res.status(response.status).json({ row: response.data });
    }
}