"use strict";

import CryptoJS from 'crypto-js';

export default class EncryptTools {
    
    static sha256({ text }) {
        return CryptoJS.SHA256(`${process.env.PASSWORD_CRYPTO_KEY}${text}${process.env.PASSWORD_CRYPTO_KEY}`).toString();
    }

    static sha512({ text }) {
        return CryptoJS.SHA512(`${process.env.PASSWORD_CRYPTO_KEY}${text}${process.env.PASSWORD_CRYPTO_KEY}`).toString();
    }

    static randomString({ length }) {
        let output           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for ( let i = 0; i < length; i++ ) {
            output += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return output;
    }

    static encryptAES({ text, key }) {
        return CryptoJS.AES.encrypt(text, key).toString();
    }
    
    static decryptAES({ cipherText, key }) {
        let bytes  = CryptoJS.AES.decrypt(cipherText, key);
        return bytes.toString(CryptoJS.enc.Utf8);
    }

    static isOdd = number => {
        return number % 2;
    }

    static isEven = number => {
        return !isOdd(number);
    }

    static hexadecimalToArrayBase10({ hexString = "", step = 2 }) {
        let outputArray = []
        
        for (let i = 0; i < hexString.length; i += step) {
            outputArray.push( parseInt(hexString.substr(i, 2), 16) );
        }

        return outputArray
    }

    static hexadecimalToBase64({ hexString = "" }) {
        return Buffer.from(hexString, 'hex').toString('base64');
    }

    static base64ToHexadecimal({ base64String }) {
        return Buffer.from(base64String, 'base64').toString('hex')
    }

    static binaryToBase64({ binaryString }) {
        return Buffer.from(binaryString, 'binary').toString('base64');
    }

    static binaryToHexadecimal({ binaryString }) {
        return Buffer.from(binaryString, 'binary').toString('hex');
    }

    static base64ToUtf8({ base64String }) {
        return Buffer.from(base64String, 'base64').toString('utf8');
    }

    static utf8ToBase64({ utf8String }) {
        return Buffer.from(utf8String, 'utf8').toString('base64');
    }
}