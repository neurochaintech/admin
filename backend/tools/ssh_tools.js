"use strict";

import * as child from 'child_process';
import { NodeSSH } from "node-ssh";

import LogsTools from "./logs_tools.js";
import ShellTools from './shell_tools.js';

export default class SshTools {
    static async send({ hostname, username, sshKeyPath, command = "", remotePath = "", timeout = 5 * 1000 }) {
        if( hostname && username && sshKeyPath && command ) {
            return new Promise((resolve, reject) => {
                const ssh = new NodeSSH();
                
                ssh.connect({
                    host: hostname,
                    username: username,
                    privateKey: sshKeyPath,
                })
                .then(function() {
                    ssh.execCommand(command, { cwd: remotePath, timeout: timeout }).then(function(result) {
                        resolve(result);
                    });
                    LogsTools.write({ element: `SshTools: ${username}@${hostname} ${command}`, textColour: LogsTools.TextColours.Yellow });
                });
            }).catch(function(error) {
                LogsTools.write({ element: `SshTools: send ERROR`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: `SshTools: ${username}@${hostname} ${command}`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: error });

                reject("");
            });
        }

        return null;
    }

    static async copyFrom({ hostname, username, sshKeyPath, remotePath, localPath, timeout = 5 * 1000 }) {
        const command = `scp -o "StrictHostKeyChecking no" -i ${sshKeyPath} ${username}@${hostname}:${remotePath} ${localPath}`;
        const stdout = ShellTools.execute({ command: command, timeout: timeout });
        return stdout
    }

    static async copyTo({ hostname, username, sshKeyPath, remotePath, localPath, timeout = 5 * 1000 }) {
        const command = `scp -o "StrictHostKeyChecking no" -r -i ${sshKeyPath} -r ${localPath} ${username}@${hostname}:${remotePath}`;
        const stdout = ShellTools.execute({ command: command, timeout: timeout });
        return stdout
    }

    static async getIpv4({ hostname, username, sshKeyPath }) {
        let ipv4 = "";
        
        try {
            const ipResponse = await this.send({
                hostname: hostname, 
                username: username,
                sshKeyPath: sshKeyPath,
                command: "hostname -I"
            });
            if(ipResponse.code === 0) {
                const ips = ipResponse.stdout.split(" ");
                ipv4 = ips[0];
            }
        } catch (err) {
            console.error(err)
        }
        
        return ipv4;
    }

    static async executeOnDockerContainer({ hostname, username, sshKeyPath, containerId, action, options = {} }) {
        let response = { status: 400, data: "" };
        
        if( ["start", "stop", "restart"].includes(action) ) {
            response = await this.send({
                hostname: hostname, 
                username: username,
                sshKeyPath: sshKeyPath,
                command: `docker ${action} ${containerId}`
            });
            // TODO formalise 
        } else if( action === "logs" ) {
            const elementsPerPage = options.elementsPerPage ? options.elementsPerPage : 20;
            const feedback = await this.send({
                hostname: hostname, 
                username: username,
                sshKeyPath: sshKeyPath,
                command: `docker logs --tail ${elementsPerPage} ${containerId}`
            });
            if(feedback.code === 0) {
                response.data = feedback.stderr.split("\n");
            } else {
                response.data = feedback.stderr;
            }
        }
        
        return response;
    }
}