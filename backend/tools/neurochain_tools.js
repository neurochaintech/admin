"use strict";

import axios from 'axios';
import elliptic from "elliptic"

import LogsTools from './logs_tools.js';
import FileSystemTools from "./fileSystem_tools.js"
import EncryptTools from './encrypt_tools.js';

export default class NeurochainTools {
    // #region Paths

    static getConfigPath({ networkId }) {
        return `${process.cwd()}/inputs/networks/${networkId}/config.json`;
    }

    static getSshKeyPath({ networkId }) {
        return `${process.cwd()}/inputs/networks/${networkId}/sshKey.pem`;
    }

    static getUrlAPI({ hostname }) {
        return `http://${hostname}:8080`;
    }

    // #endregion Paths
    
    // #region Config JSON

    static getNetworks() {
        let output = [];

        const path = `${process.cwd()}/inputs/networks`;
        const dirsPaths = FileSystemTools.getDirectoriesPaths({ inputPath: path, recursive: false });

        for( const dirPath of dirsPaths ) {
            const elements = dirPath.split("/");
            output.push( {
                id: elements[elements.length-1] 
            } );
        }

        return output;
    }

    static getNodes({ networkId }) {
        let output = [];

        const configPath = this.getConfigPath({ networkId: networkId });
        const nodes = FileSystemTools.readJson({ inputPath: configPath });
        
        for (const [key, value] of Object.entries(nodes) ) {
            let node = value;
            node.id = key;
            node.sshHelp = `ssh -i "${this.getSshKeyPath({ networkId: networkId })}" ${node.username}@${node.hostname}`;
            output.push( node );
        }

        return output;
    }

    static getNode({ networkId, nodeId }) {
        let output = {};

        const configPath = this.getConfigPath({ networkId: networkId });
        const networkConfig = FileSystemTools.readJson({ inputPath: configPath });

        if( nodeId in networkConfig ) {
            output = networkConfig[nodeId];
            output.id = nodeId;
            output.sshHelp = `ssh -i "${this.getSshKeyPath({ networkId: networkId })}" ${output.username}@${output.hostname}`;
        }

        return output;
    }

    // #endregion Config JSON

    // #region Blocks

    static async getBlocksCount({ hostname }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/total_nb_blocks`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getBlocksCount ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getLatestsBlocks({ hostname, elementsPerPage }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/last_blocks/${elementsPerPage}`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getLatestsBlocks ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getBlockById({ hostname, id }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/block/id`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, { data: id });
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getBlockById ${id} ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getBlockByIndex({ hostname, index }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/block/height/${index}`;

        let output = { status: 400, data: {} };
        try {
            // LogsTools.write({ element: `NeurochainTools: ${url}`, textColour: LogsTools.TextColours.Blue });
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getBlockByIndex ${index} ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    // #endregion Blocks

    // #region Transactions

    static async getTransactionsCount({ hostname }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/total_nb_transactions`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getTransactionsCount ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getTransaction({ hostname, id }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/transaction`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, { data: id });
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getTransaction ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    // The documention says it works as an OR if both output and input are given but nothing is returned
    // static async getTransactions({ hostname, publicKey, page = 0, elementsPerPage = 20 }) {
    //     const urlAPI = this.getUrlAPI({ hostname: hostname });
    //     const url = `${urlAPI}/list_transactions`;
    
    //     let output = { status: 400, data: {} };
    //     try {
    //         const response = await axios.post(url, { 
    //             page: page,
    //             page_size: elementsPerPage,
    //             input_key_pub: {rawData: publicKey},
    //             output_key_pub: {rawData: publicKey}
    //         });
    //         output.status = response.status;
    //         output.data = response.data;
    //     } catch (e) {
    //         LogsTools.write({ element: `NeurochainTools: getTransactions ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: e });
    //     }

    //     return output;
    // }

    static async getTransactionsAuthor({ hostname, publicKey, page = 0, elementsPerPage = 10 }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/list_transactions`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, { 
                page: page,
                page_size: elementsPerPage,
                input_key_pub: {rawData: publicKey}
            });
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getTransactionsAuthor ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getTransactionsRecipient({ hostname, publicKey, page = 0, elementsPerPage = 10 }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/list_transactions`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, { 
                page: page,
                page_size: elementsPerPage,
                output_key_pub: {rawData: publicKey}
            });
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getTransactionsRecipient ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async isKeyValid({ hostname, key }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/validate`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, {"rawData": `${key}`});
            if(response.status === 200 ) {
                output.status = 200
                output.data.validated = response.data.length === 0;
            } else {
                LogsTools.write({ element: `NeurochainTools: isKeyValid ${key} ERROR`, textColour: LogsTools.TextColours.Red });
            }
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: isKeyValid ${key} ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async createTransaction({ hostname, 
        authorPublicKey, recipientPublicKey,
        value, data, fee }) {
        let output = { status: 400, data: {} };

        let transaction;
        if( authorPublicKey && recipientPublicKey && value && data) {
            try {
                transaction = {
                    key_pub: {rawData: authorPublicKey},
                    outputs: [
                        {
                            key_pub: {rawData: recipientPublicKey},
                            value: {value: value.toString()},
                            data: Buffer.from( JSON.stringify(data) ).toString("base64"),
                        }
                    ],
                    fee: fee
                };

                const urlAPI = this.getUrlAPI({ hostname: hostname });
                const url = `${urlAPI}/create_transaction`;
            
                const response = await axios.post(url, transaction);

                if( response.status === 200 && response.data.length > 0 ) {
                    output.status = 200;
                    output.data = {
                        payload: response.data
                    }
                } else {
                    output.error = response.data;
                    LogsTools.write({ element: `NeurochainTools: createTransaction ERROR`, textColour: LogsTools.TextColours.Red });
                    LogsTools.write({ element: response });
                }
            } catch (e) {
                LogsTools.write({ element: `NeurochainTools: createTransaction ERROR`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: transaction });
                LogsTools.write({ element: e });
            }
        } else {
            output.data.error = "CREATETRANSACTION - Missing elements, required: authorPublicKey, authorPrivateKey, recipientPublicKey, value, data, fee";
        }

        return output;
    }

    static signTransaction({ authorPrivateKey, payload }) {
        let signature = "";

        if( authorPrivateKey && payload ) {
            const cleanKey = this.sanitizeKeyHex({ key: authorPrivateKey });
            signature = this.signPayload({ 
                privateKey: cleanKey, 
                payload: payload  
            });
        }
     
        return signature;
    }

    static async publishTransaction({ hostname, authorPublicKey, payload, signature }) {
        let output = { status: 400, data: {} };

        if( authorPublicKey && payload && signature ) {
            try {
                const requestBody = {
                    transaction: payload,
                    signature: signature,
                    keyPub: {rawData: authorPublicKey}
                };

                const urlAPI = this.getUrlAPI({ hostname: hostname });
                const url = `${urlAPI}/publish`;
            
                const response = await axios.post(url, requestBody);
                if( response.status === 200 ) {
                    output.status = 200;
                    output.data = {
                        transactionData: response.data
                    }
                } else {
                    output.error = response.data;
                    LogsTools.write({ element: `NeurochainTools: publishTransaction ${response.status} ERROR`, textColour: LogsTools.TextColours.Red });
                    LogsTools.write({ element: response.data });
                }
            } catch (e) {
                LogsTools.write({ element: `NeurochainTools: publishTransaction ERROR`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: e });
            }
        }  else {
            output.data.error = "PUBLISHTRANSACTION - Missing elements, required: authorPublicKey, payload, signature";
        }

        return output;
    }

    /*
    Function to pulish transaction from raw elements, process the steps:
    - check keys
    - create transaction
    - sign transaction
    - publish transaction
    */
    static async sendTransaction({ hostname, 
        authorPublicKey, authorPrivateKey, recipientPublicKey,
        value, data, fee }) {
        let output = { status: 400, data: {} };

        const testAuthorValid = await this.isKeyValid({ hostname: hostname, key: authorPublicKey });
        const testRecipientValid = await this.isKeyValid({ hostname: hostname, key: recipientPublicKey });
        if( testAuthorValid.data.validated && testRecipientValid.data.validated ) {
            const transaction = { 
                hostname: hostname,
                authorPublicKey: authorPublicKey, 
                recipientPublicKey: recipientPublicKey,
                value: value, 
                data: data, 
                fee: fee 
            };
            const createResponse = await this.createTransaction(transaction);
            if( createResponse.data.payload ) {
                const signature = this.signTransaction({ authorPrivateKey: authorPrivateKey, payload: createResponse.data.payload });
                output = await this.publishTransaction({ 
                    hostname: hostname, 
                    authorPublicKey: authorPublicKey, 
                    payload: createResponse.data.payload, 
                    signature: signature 
                });
            }
        }

        return output;
    }

    // #endregion Transactions

    // #region Wallets

    static async getBalance({ hostname, publicKey }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/balance`;
        const data = {
            rawData: publicKey
        };        

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, data);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getBalance ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    // #endregion Wallets

    // #region Core

    static async getReady({ hostname }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/ready`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getReady ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getPeers({ hostname }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/peers`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getPeers ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    static async getStatus({ hostname }) {
        const urlAPI = this.getUrlAPI({ hostname: hostname });
        const url = `${urlAPI}/status`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            LogsTools.write({ element: `NeurochainTools: getStatus ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }

        return output;
    }

    // #endregion Core

    // #region Encrypt

    static compressKey({ keyBasePoint = {} }) {
        const x = keyBasePoint.x.toString("hex");

        if (keyBasePoint.y.isEven()) {
            return "02" + x;
        } else {
            return "03" + x;
        }
    }
    
    static generateKeys() {
        const ecdsa = new elliptic.ec("secp256k1");
        const keyPair = ecdsa.genKeyPair();
        const privateKey = keyPair.getPrivate("hex");
        const publicKey = compressKey(keyPair.getPublic())

        return {
            private: privateKey,
            public: EncryptTools.hexadecimalToBase64({ hexString: publicKey}),
        }
    }

    static hash({ arrayBase10 }) {
        const ecdsa = new elliptic.ec("secp256k1");
        return ecdsa.hash().update(arrayBase10).digest("hex");
    }

    static sign({ key = "", message = "" }) {
        const ecdsa = new elliptic.ec("secp256k1");
        const ecdsaKey = ecdsa.keyFromPrivate(key, "hex");
        const signature = ecdsaKey.sign(message);
        return signature.r.toString("hex", 64) + signature.s.toString("hex", 64);
    }

    static signPayload({ privateKey = "", payload = "" }) {
        const arrayBase10 = EncryptTools.hexadecimalToArrayBase10({ hexString: payload });
        const digest = this.hash({ arrayBase10: arrayBase10 });
        const signature = this.sign({ key: privateKey, message: digest });
        return signature
    }

    static sanitizeKeyHex({ key = "" }) {
        let outputString = "";

        const hexArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
        for (let iChar = 0; iChar < key.length; iChar++) {
            const char = key.charAt(iChar);
            if( hexArray.includes(char.toLowerCase()) ) {
                outputString += char.toString();
            }
        }

        return outputString;
    }
    
    static sanitizeRawKey({ key = "" }) {
        let outputString = key.replaceAll(" ", "");
        return outputString;
    }

    // #endregion Encrypt

}