"use strict";

import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import LogsTools from "./tools/logs_tools.js"
import FileSystemTools from "./tools/fileSystem_tools.js";

async function linkRouters(app) {
    const apiPath = `${process.cwd()}/api`;
    const routersPaths = FileSystemTools.getFilesPaths({ inputPath: apiPath, recursive: true, whitelist: ["_router.js"]  });
    for(const routerPath of routersPaths) {
        try {
            const apiRouter = await import(routerPath);
            app.use(`/api`, apiRouter.default);
            
            for( const layer of apiRouter.default.stack ) {
                for( const method of Object.keys(layer.route.methods) ) {
                    LogsTools.write({ element: `Router: ${method.toUpperCase()} /api${layer.route.path}`, textColour: LogsTools.TextColours.Green });
                }
            }
        } catch(e) {
            LogsTools.write({ element: e, textColour: LogsTools.TextColours.Red  });
        }
    }
}

async function createApp() {
    try {
       
        const app = express();

        // #region Data

        app.use(express.json());
        app.use(express.urlencoded({
            extended: true
        }));

        // #endregion Data

        // #region Security

        app.use(cors());
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());

        // #endregion Security

        // #region Routers

        await linkRouters(app);

        // #endregion Routers

        // #region Default error handler

        app.use(function (req, res) {
            if(!res.headersSent) {
                LogsTools.write({ element: `Default error handler for ${req.protocol}://${req.get('host')}${req.originalUrl}`, textColour: LogsTools.TextColours.Red });
                //LogsTools.write({ element: req.body });

                res.status(400).json({
                    message: "Wrong inputs. Please check the documentation or contact administors."
                });
            }
        });
        
        // #endregion  Default error handler
        
        return app;

    } catch(e) {
        LogsTools.write({ element: e, textColour: LogsTools.TextColours.Red  });
        process.exit(2);
    }
};

const Backend = await createApp();
export default Backend;
