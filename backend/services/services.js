import axios from 'axios';

export default class Services {
    static COLLECTION_NAME = "";

    static getBackendUri() {
        return process.env.REACT_APP_BACKEND_URI;
    }
    
    static async list({ parameters = {} }) {
        let params = [];
        for( const [key, value] of Object.entries(parameters) ) {
            params.push(`${key}=${encodeURIComponent(value)}`);
        }
        const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}?${ params.join("&") }`;
        // console.log(url);
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }
    
    static async create({ data }) {
        const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, data);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async read({ id }) {
        const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}/${encodeURIComponent(id)}`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async update({ id, data }) {
        const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}/${encodeURIComponent(id)}`;
        
        let output = { status: 400, data: {} };
        try {
            const response = await axios.put(url, data);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async delete({ id, data }) {
        const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}/${encodeURIComponent(id)}`;
        
        let output = { status: 400, data: {} };
        try {
            const response = await axios.delete(url, { data: data });
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }
        
        return output;
    }

    // static async count({ parameters = {} }) {
    //     let params = [];
    //     for( const [key, value] of Object.entries(parameters) ) {
    //         params.push(`${key}=${encodeURIComponent(value)}`);
    //     }
    //     const url = `${this.getBackendUri()}/${this.COLLECTION_NAME}/count?${ params.join("&") }`;
            
    //     let output = { status: 400, data: {} };
    //     try {
    //         const response = await axios.get(url);
    //         output.status = response.status;
    //         output.data = response.data;
    //     } catch (e) {
    //         output.status = e.response.status;
    //         output.data = e.response.data;
    //     }

    //     return output;
    // }
}
