"use strict";

import { format } from 'date-fns'
import fs from "fs";

import LogsTools from "./tools/logs_tools.js"
import NeurochainTools from "./tools/neurochain_tools.js";
import ShellTools from "./tools/shell_tools.js";
import SshTools from "./tools/ssh_tools.js";

const SECONDS = 1000;
const LOOP_DELAY = 20 * SECONDS;
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
} 

const MIN_SENT = 1000;
let NETWORK_ID = "testnet6";
let NODES_DATA = {};

// TODO : Test with generated wallets and not node wallet

async function saveLogsAndStopCore({ node }) {
    const rawLogs = await SshTools.executeOnDockerContainer({ 
        hostname: node.hostname,
        username: node.username, 
        sshKeyPath: NeurochainTools.getSshKeyPath({ networkId: NETWORK_ID }),
        action: "logs",
        containerId: "core",
        options: { elementsPerPage: 500 }
    });
    const logs = rawLogs.data;
    const date = format(new Date().getTime(), 'yyyyMMdd');
    const filePath = `${process.cwd()}/logs/${node.id}_${date}.log`;
    const writeStream = fs.createWriteStream(filePath, {flags: 'a'});
    redirectedConsole.log(logs);
    writeStream.end("");

    await SshTools.executeOnDockerContainer({ 
        hostname: node.hostname,
        username: node.username, 
        sshKeyPath: NeurochainTools.getSshKeyPath({ networkId: NETWORK_ID }),
        action: "stop",
        containerId: "core"
    });
}

async function stressNetwork({ nodes }) {
    let loop = true;

    LogsTools.write({ element: `# STRESS NETWORK`, textColour: LogsTools.TextColours.Yellow });
    for( const node of nodes ) {
        LogsTools.write({ element: `## ${node.id}: ${node.username}@${node.hostname}`, textColour: LogsTools.TextColours.Yellow });

        const previousBlocksCount = NODES_DATA[node.hostname].blocks;
        const blocksCount = (await NeurochainTools.getBlocksCount({ hostname: node.hostname })).data;
        if( previousBlocksCount < blocksCount ) {
            LogsTools.write({ element: `blocksCount: ${previousBlocksCount} -> ${blocksCount}`, textColour: LogsTools.TextColours.Green});

            NODES_DATA[node.hostname].blocks = blocksCount;

            // const transactionsCount = (await NeurochainTools.getTransactionsCount({ hostname: node.hostname })).data;
            // LogsTools.write({ element: `transactionsCount: ${transactionsCount}`, textColour: LogsTools.TextColours.Blue});
            
            const wallet = NODES_DATA[node.hostname].wallet;
            let balance = (await NeurochainTools.getBalance({ hostname: node.hostname, publicKey: wallet.publicKey })).data.value;
            balance = parseFloat(balance);
            
            if( balance > MIN_SENT ) {
                const iReceiver = getRandomInt(nodes.length);
                const receiver = nodes[iReceiver];
                const tx = await NeurochainTools.sendTransaction({ 
                    hostname: node.hostname, 
                    authorPublicKey: wallet.publicKey, 
                    authorPrivateKey: wallet.privateKeyExponent, 
                    recipientPublicKey: NODES_DATA[receiver.hostname].wallet.publicKey,
                    value: MIN_SENT, 
                    data: `${node.id} ${node.hostname}: ${Date.now}`, 
                    fee: 0 
                });
                LogsTools.write({ element: `Transaction ${tx.data.transactionData.id.data}: ${node.id} ${node.hostname} -> ${MIN_SENT} -> ${receiver.id} ${receiver.hostname}`, textColour: LogsTools.TextColours.Green});
            }

            const balanceStr = balance.toLocaleString();
            LogsTools.write({ element: `Balance for ${wallet.publicKey}: ${balanceStr} ${balance < Number.MAX_SAFE_INTEGER} (MAX_SAFE_INTEGER: ${Number.MAX_SAFE_INTEGER.toLocaleString()})`, textColour: LogsTools.TextColours.Blue});

        } else {
            LogsTools.write({ element: `blocksCount: ${previousBlocksCount} -> ${blocksCount}`, textColour: LogsTools.TextColours.Red});
            loop = false;

            await saveLogsAndStopCore({ node: node });
        }
    }

    if(loop) {
        LogsTools.write({ element: `Waiting ${LOOP_DELAY.toLocaleString()} ms...`, textColour: LogsTools.TextColours.Blue});
        await sleep(LOOP_DELAY);
        await stressNetwork({ nodes: nodes });
    }
}

async function main() {
    try {
        const nodes = NeurochainTools.getNodes({ networkId: NETWORK_ID });
        for( const node of nodes ) {
            const keysPath = `${process.cwd()}/inputs/networks/${NETWORK_ID}/${node.id}/keys`;
            const wallet = JSON.parse( await ShellTools.execute({ command: `${process.cwd()}/tools/ECDSA -decipher -privateKeyPath "${keysPath + `/key.priv`}" -publicKeyPath "${keysPath + `/key.pub`}"` }) );

            NODES_DATA[node.hostname] = { 
                blocks: 0,
                wallet: wallet
            };
        }
        await stressNetwork({ nodes: nodes });

        process.exit(0);
    } catch(e) {
        LogsTools.write({ element: e, textColour: LogsTools.TextColours.Red  });
        process.exit(1);
    }
};
main();
// npm run stress