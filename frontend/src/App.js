import { BrowserRouter, Navigate, Routes, Route } from "react-router-dom";

import "./styles/MainStructure.css";

import MainSidebar from "./components/common/MainSidebar";

import NetworksRouter from "./components/networks/NetworksRouter";
import BlockchainRouter from "./components/blockchain/BlockchainRouter";
import AdminRouter from "./components/admin/AdminRouter";

export default function App() {

    return (
        <div className='App'>

            <div id="content">

                <MainSidebar />
                
                <div id="mainContent">
                    <div className="margin-top-10">
                        <BrowserRouter>
                            <Routes>
                                <Route path="/">
                                    <Route index element={<Navigate to="/networks/status" replace />} />
                                    
                                    {NetworksRouter}
                                    {BlockchainRouter}
                                    {AdminRouter}
                                    
                                    <Route path="*" element={<Navigate to="/networks/status" replace />} />
                                </Route>
                            </Routes>
                        </BrowserRouter>
                    </div>
                </div>

            </div> 

        </div>
    );
}
