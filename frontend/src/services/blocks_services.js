import axios from 'axios';

export default class BlocksServices {
    static getUrlRadix({ networkId, nodeId }) {
        return `${process.env.REACT_APP_BACKEND_URI}/networks/${networkId}/nodes/${nodeId}/blocks`;
    }
    
    static async count({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/count`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async loadById({ networkId, nodeId, id }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/${id}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async loadBlockByIndex({ networkId, nodeId, index }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/index/${index}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }
}