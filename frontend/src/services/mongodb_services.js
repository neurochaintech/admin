import axios from 'axios';

export default class MongoDBServices {
    static API_PORT = 5822;
    static getUrlRadix({ hostname }) {
        return `http://${hostname}:${this.API_PORT}/api`;
    }
    
    static async branches({ hostname }) {
        let url = `${this.getUrlRadix({ hostname })}`;
        url += `/blocks/branches`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }
}