import axios from 'axios';

export default class TransactionsServices {
    static getUrlRadix({ networkId, nodeId }) {
        return `${process.env.REACT_APP_BACKEND_URI}/networks/${networkId}/nodes/${nodeId}/transactions`;
    }
    
    static async count({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/count`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async loadById({ networkId, nodeId, id }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/${encodeURIComponent(id)}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async send({ networkId, nodeId, transaction }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.post(url, transaction);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async listAuthor({ networkId, nodeId, publicKey }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/authors/${encodeURIComponent(publicKey)}`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async listRecipient({ networkId, nodeId, publicKey }) {
        let url = `${this.getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
        url += `/recipients/${encodeURIComponent(publicKey)}`;

        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }
}