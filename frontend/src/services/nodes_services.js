import axios from 'axios';

export default class NodesServices {
    static getUrlRadix({ networkId }) {
        return `${process.env.REACT_APP_BACKEND_URI}/networks/${networkId}/nodes`;
    }

    static async list({ networkId }) {
        const url = `${this.getUrlRadix({ networkId: networkId })}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async dockers({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/dockers`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async status({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/status`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async ready({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/ready`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async peers({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/peers`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async coreLogs({ networkId, nodeId, lines = 20 }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/core/logs?elementsPerPage=${lines}`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async wallet({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/wallet`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async botJson({ networkId, nodeId }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/botJson`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }

    static async balance({ networkId, nodeId, publicKey }) {
        let url = `${this.getUrlRadix({ networkId: networkId })}`;
        url += `/${nodeId}/wallet/${ encodeURIComponent(publicKey) }/balance`;
            
        let output = { status: 400, data: {} };
        try {
            const response = await axios.get(url);
            output.status = response.status;
            output.data = response.data;
        } catch (e) {
            output.status = e.response.status;
            output.data = e.response.data;
        }

        return output;
    }
}