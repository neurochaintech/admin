import MainComponent from "../common/MainComponent";

import NetworksServices from "../../services/networks_services";
import NodesServices from "../../services/nodes_services";
import TransactionsServices from "../../services/transactions_services";
import Transaction from "../blockchain/Transaction";
import BlocksServices from "../../services/blocks_services";
import Block from "../blockchain/Block";
import MongoDBServices from "../../services/mongodb_services";

export default class BlockchainHealth extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networks: [],
            nodes: [],
            selectedNetworkId: "",
            selectedNodeId: "",

            transactionId: "",
            transactions: [],
            blockId: "",
            blocks: [],
            publicKey: "",

            branches: []
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.networks.length === 0 ) {
                this.loadNetworks();
            }
            if( this.state.selectedNetworkId.length > 0 && this.state.nodes.length === 0 ) {
                this.loadNodes();
            }

            if(this.state.selectedNetworkId.length > 0 
                && this.state.selectedNodeId.length > 0 
                && this.state.branches.length === 0 
                ) {
                this.loadBranches();
            }
        }
    }

    async loadNetworks() {
        const response = await NetworksServices.list({ parameters: {} });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"networks": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadNodes() {
        const response = await NodesServices.list({ networkId: this.state.selectedNetworkId });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"nodes": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadBranches() {
        let hostname;
        for( const node of this.state.nodes ) {
            if( node.id === this.state.selectedNodeId ) {
                hostname = node.hostname;
            }
        }
        
        const response = await MongoDBServices.branches({ hostname: hostname })
        if( response.status === 200 ) {
            super.updateState({ inputs: {"branches": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    networkSelected(event) {
        event.preventDefault();

        const networkId = event.target.value;
        super.updateState({ inputs: {"selectedNetworkId": networkId, "nodes": [], selectedNodeId: ""} });
    }

    nodeSelected(event) {
        event.preventDefault();

        const nodeId = event.target.value;
        super.updateState({ inputs: {"selectedNodeId": nodeId} });
    }

    async handleChange(event) {
        event.preventDefault();

        let obj = {};
        obj[event.target.name] = event.target.value;

        if( event.target.name.includes("Id") || event.target.name.includes("Key") ) {
            obj[event.target.name] = event.target.value.replaceAll(' ', '');
        }

        super.updateState({ inputs: obj });
    }

    render() {
        return <>
            <h2>Health</h2>

            <div className='row'>
                <div className='col-1 vertical-center label'>
                    Network:
                </div>
                <div className='col-5'>
                    <form>
                        <select onChange={event => this.networkSelected(event)}>
                            <option value="">Please select one</option>
                            { this.state.networks.length > 0 && this.state.networks.map((network, index) => ( 
                                <option value={network.id} key={`network_${index}`}>
                                    {network.id}
                                </option>
                            ))}
                        </select>
                    </form>
                </div>

                { this.state.nodes.length > 0 &&
                    <>
                        <div className='col-1 vertical-center label'>
                            Node:
                        </div>
                        <div className='col-5'>
                            <form>
                                <select onChange={event => this.nodeSelected(event)}>
                                    <option value="">Please select one</option>
                                    { this.state.nodes.map((node, index) => ( 
                                        <option value={node.id} key={`node_${index}`}>
                                            {node.id}
                                        </option>
                                    ))}
                                </select>
                            </form>
                        </div>
                    </>
                }
            </div>
            
            <h3>Branches</h3>
            <table className="table">
                <thead>
                    <tr>
                        <th className="text-right">Name&nbsp;</th>
                        <th>&nbsp;Count</th>
                    </tr>
                </thead>
                <tbody>
                    { this.state.branches.map((branch, index) =>
                        <tr key={`branch_${index}`}>
                            <td className="text-right">{branch.name}&nbsp;</td>
                            <td>&nbsp;{new Intl.NumberFormat(process.env.REACT_APP_LOCALE).format(branch.count)}</td>
                        </tr>
                    )}
                </tbody>
            </table>
            
            {super.render()}
        </>
    }
}