import { Route } from "react-router-dom";
// import BlockchainHealth from "./BlockchainHealth";
import Cheatsheets from "./Cheatsheets";

const AdminRouter = ( 
    <Route path="admin/api">
        {/* <Route path="health" element={<BlockchainHealth />} /> */}
        <Route path="cheatsheets" element={<Cheatsheets />} />
    </Route>
);
export default AdminRouter;