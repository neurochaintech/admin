import MainComponent from "../common/MainComponent";

export default class Cheatsheets extends MainComponent {
    
    render() {
        return <>
        
        <h2>Cheatsheets</h2>
        
        <h3>Docker</h3>
        
        <p>docker container list --all</p>
        <p>docker logs --follow --tail 20 containerId/containerName</p>
        <p>docker exec -it containerId/containerName bash</p>

        <h3>Install Core</h3>

        <p>cd ansible</p>
        <p>ansible-playbook -e "file_prefix=testnet6 core_version=7e2230ee24aeb7693c1d411b0790aa47cd6217e5" -i testnet6/ips.yml main.yml -kK</p>
        <p>ansible-playbook -i testnet6/ips.yml ~/ansible/99_clean.yml -kK</p>
        
        </>
    }
}
