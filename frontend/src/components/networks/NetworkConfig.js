import axios from 'axios';

import MainComponent from "../common/MainComponent";

export default class NetworkConfig extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networkId: "",
            config: ""
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.props.networkId.length > 0 && this.state.networkId !== this.props.networkId ) {
            this.loadData(this.props.networkId);
        }
    }

    async loadData(networkId) {
        const response = await axios.get(`${process.env.REACT_APP_BACKEND_URI}/networks/${networkId}/config`);

        if( response.status === 200 ) {
            super.updateState({ inputs: {
                "config": JSON.stringify(response.data.rows, null, 4),
                "networkId": networkId
            } });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async handleChange(event) {
        event.preventDefault();

        let obj = {};
        obj[event.target.name] = event.target.value;

        super.updateState({ inputs: obj });
    }

    async handleSubmit(event) {
        event.preventDefault();

        const response = await axios.put(`${process.env.REACT_APP_BACKEND_URI}/networks/${this.state.networkId}/config`, {
            config: this.state.config
        });

        if( response.status === 200 ) {
            super.updateState({ inputs: {
                "info": "",
                "success": `Config updated`
            } });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        return <>

            <h3>{this.state.networkId}</h3>

            { this.state.networkId.length > 0 &&
            <form className="form" onSubmit={event => this.handleSubmit(event)}>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Config:
                    </div>
                    <div className='col-10'>
                        <textarea name="config" 
                            value={this.state.config} 
                            onChange={event => this.handleChange(event)}/>
                    </div>
                </div>

                <input type="submit" value="Save" />
            </form>
            }

            {super.render()}
        </>
    }
}