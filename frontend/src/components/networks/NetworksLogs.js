import NetworksList from "./NetworksList";
import NodeLogs from "../nodes/NodeLogs";

export default class NetworksLogs extends NetworksList {
    // constructor() {
    //     super();
    // }

    render() {
        return <>
            <h2>Logs</h2>

            {super.render()}

            { this.state.nodes.length > 0 &&
                <div className="margin-top-20">
                    <h3>Nodes: </h3>

                    { this.state.nodes.map((node, index) => ( 
                        <NodeLogs networkId={this.state.selectedNetworkId} node={node} key={`node_${index}`}/>
                    ))}
                </div>
            }
        </>
    }
}