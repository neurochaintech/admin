import axios from 'axios';

import NetworksList from './NetworksList';
import NetworkConfig from './NetworkConfig';

export default class NetworksConfig extends NetworksList {
    constructor() {
        super();

        const jsonConfig = {
            "Bot-testnet-1": {
                "username": "ubuntu",
                "hostname": "ec2-15-188-110-204.eu-west-3.compute.amazonaws.com"
            },
            "Bot-testnet-2": {
                "username": "ubuntu",
                "hostname": "ec2-15-188-28-95.eu-west-3.compute.amazonaws.com"
            },
            "Bot-testnet-3": {
                "username": "ubuntu",
                "hostname": "ec2-15-188-99-38.eu-west-3.compute.amazonaws.com"
            },
            "Bot-testnet-4": {
                "username": "ubuntu",
                "hostname": "ec2-15-188-198-103.eu-west-3.compute.amazonaws.com"
            },
            "Bot-testnet-5": {
                "username": "ubuntu",
                "hostname": "ec2-35-181-28-116.eu-west-3.compute.amazonaws.com"
            }
        }

        const privateProps = {
            name: "testnet",
            config: JSON.stringify(jsonConfig, null, 4),
            pemFile: {}
        }
        this.state = {...this.state, ...privateProps};
    }

    async handleChange(event) {
        event.preventDefault();

        let obj = {};
        obj[event.target.name] = event.target.value;

        if( event.target.name === "pemFile" )  {
            console.log(event.target.name, event.target.files)
            obj[event.target.name] = event.target.files[0];
        }        

        super.updateState({ inputs: obj });
    }

    async handleSubmit(event) {
        event.preventDefault();

        const formData = new FormData();
        formData.append("name", this.state.name);
        formData.append("config", this.state.config);
        formData.append("pemFile", this.state.pemFile);
        const response = await axios.post(`${process.env.REACT_APP_BACKEND_URI}/networks`, formData, {
            headers: {
                "content-type": "multipart/form-data"
            },
        });

        if( response.status === 201 ) {
            super.updateState({ inputs: {
                "info": "",
                "success": `Network created`
            } });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        return <>
            <h2>Config</h2>

            <h3>Add</h3>

            <form className="form" onSubmit={event => this.handleSubmit(event)}>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Name:
                    </div>
                    <div className='col-10'>
                        <input type="string" name="name" value={this.state.name} onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        PEM:
                    </div>
                    <div className='col-10'>
                        <input type="file" name="pemFile" accept=".pem" onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Config:
                    </div>
                    <div className='col-10'>
                        <textarea name="config" 
                            value={this.state.config} 
                            onChange={event => this.handleChange(event)}/>
                    </div>
                </div>

                <input type="submit" value="Save" />
            </form>

            <h3>Update</h3>

            {super.render()}

            <NetworkConfig networkId={this.state.selectedNetworkId}/>
        </>
    }
}