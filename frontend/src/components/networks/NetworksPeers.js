import NetworksList from "./NetworksList";
import NodePeers from "../nodes/NodePeers";

export default class NetworksPeers extends NetworksList {
    // constructor() {
    //     super();
    // }

    render() {
        return <>
            <h2>Peers</h2>

            {super.render()}

            { this.state.nodes.length > 0 &&
                <div className="margin-top-20">
                    <h3>Nodes: </h3>

                    { this.state.nodes.map((node, index) => ( 
                        <NodePeers networkId={this.state.selectedNetworkId} node={node} key={`node_${index}`}/>
                    ))}
                </div>
            }
        </>
    }
}