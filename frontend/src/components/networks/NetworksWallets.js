import NetworksList from "./NetworksList";
import NodeWallet from "../nodes/NodeWallet";

export default class NetworksWallets extends NetworksList {
    // constructor() {
    //     super();
    // }

    render() {
        return <>
            <h2>Wallets</h2>

            {super.render()}

            { this.state.nodes.length > 0 &&
                <div className="margin-top-20">
                    <h3>Nodes: </h3>

                    { this.state.nodes.map((node, index) => ( 
                        <NodeWallet networkId={this.state.selectedNetworkId} node={node} key={`node_${index}`}/>
                    ))}
                </div>
            }
        </>
    }
}