import NetworksList from "./NetworksList";
import NodeStatus from "../nodes/NodeStatus";

export default class NetworksStatus extends NetworksList {
    // constructor() {
    //     super();
    // }

    render() {
        return <>
            <h2>Status</h2>

            {super.render()}

            { this.state.selectedNetworkId.length > 0 && this.state.nodes.length > 0 &&
                <div className="margin-top-20">
                    <h3>Nodes: </h3>

                    { this.state.nodes.map((node, index) => ( 
                        <NodeStatus networkId={this.state.selectedNetworkId} node={node} key={`node_${index}`}/>
                    ))}
                </div>
            }
        </>
    }
}