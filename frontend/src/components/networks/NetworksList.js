import MainComponent from "../common/MainComponent";

import NetworksServices from "../../services/networks_services";
import NodesServices from "../../services/nodes_services";

export default class NetworksList extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networks: [],
            selectedNetworkId: "",
            nodes: []
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.networks.length === 0 ) {
            this.loadNetworks();
        }
        if( this.state.selectedNetworkId.length > 0 && this.state.nodes.length === 0 ) {
            this.loadNodes();
        }
    }

    async loadNetworks() {
        const response = await NetworksServices.list({ parameters: {} });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"networks": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadNodes() {
        const response = await NodesServices.list({ networkId: this.state.selectedNetworkId });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"nodes": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    networkSelected(event) {
        event.preventDefault();

        const networkId = event.target.value;
        super.updateState({ inputs: {
            "selectedNetworkId": networkId,
            nodes: []
        } });
    }
    
    render() {
        return <>
            <h3>Networks:</h3>
            
            <div className='row'>
                <div className='col-12'>
                    <form>
                        <select onChange={event => this.networkSelected(event)}>
                            <option value="">Please select one</option>
                            { this.state.networks.length > 0 && this.state.networks.map((network, index) => ( 
                                <option value={network.id} key={`network_${index}`}>
                                    {network.id}
                                </option>
                            ))}
                        </select>
                    </form>
                </div>
            </div>
            

            {super.render()}
        </>
    }
}