import { Route } from "react-router-dom";

import NetworksBotJson from "./NetworksBotJson";
import NetworksDockers from "./NetworksDockers";
import NetworksLogs from "./NetworksLogs";
import NetworksPeers from "./NetworksPeers";
import NetworksStatus from "./NetworksStatus";
import NetworksWallets from "./NetworksWallets";
import NetworksConfig from "./NetworksConfig";

const NetworksRouter = ( 
    <Route path="networks">
        <Route path="config" element={<NetworksConfig />} />
        <Route path="dockers" element={<NetworksDockers />} />
        <Route path="status" element={<NetworksStatus />} />
        <Route path="peers" element={<NetworksPeers />} />
        <Route path="bots-json" element={<NetworksBotJson />} />
        <Route path="wallets" element={<NetworksWallets />} />
        <Route path="logs" element={<NetworksLogs />} />
    </Route>
);
export default NetworksRouter;