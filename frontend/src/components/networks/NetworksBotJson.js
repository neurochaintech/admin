import NetworksList from "./NetworksList";
import NodeBotJson from "../nodes/NodeBotJson";

export default class NetworksBotJson extends NetworksList {
    // constructor() {
    //     super();
    // }

    render() {
        return <>
            <h2>bot.json</h2>

            {super.render()}

            { this.state.nodes.length > 0 &&
                <div className="margin-top-20">
                    <h3>Nodes: </h3>

                    { this.state.nodes.map((node, index) => ( 
                        <NodeBotJson networkId={this.state.selectedNetworkId} node={node} key={`node_${index}`}/>
                    ))}
                </div>
            }
        </>
    }
}