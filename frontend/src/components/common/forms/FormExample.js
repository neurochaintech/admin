import InputsGenerator from "./InputsGenerator";

export default class FormExample extends MainComponent {
    formFields = [
        { label:'A text', name: 'text', type: 'text' },
        { label:'A date', name: 'date', type: 'date' },
        { label:'A number', name: 'number', type: 'number', min: '0', max: '10' },
        { label:'A range', name: 'range', type: 'range' },
        { label:'A select', name: 'select', type: 'select', inputs: [
            { label:'Please select an option', value: '' },
            { label:'option select 1', value: 'optionSelect1' },
            { label:'option select 2', value: 'optionSelect2' },
        ] },
        { label:'A radio', name: 'radio', type: 'radio', inputs: [
            { label:'option radio 1', value: 'optionRadio1' },
            { label:'option radio 2', value: 'optionRadio2' },
        ] },
        { label:'A checkbox', name: 'checkbox', type: 'checkbox', inputs: [
            { label:'option checkbox 1', value: 'optionCheckbox1' },
            { label:'option checkbox 2', value: 'optionCheckbox2' },
        ] },

        { label:'Email Address', name: 'emailAddress', type: 'email' },
        { label:'Password', name: 'password', type: 'password'  },
        { label:'Log in', type: 'submit'  },
    ]

    render() {
        return <>
        <h1>Form</h1>

        <div className="margin-top-30">
            
            <form className="form">
                <InputsGenerator fields={this.formFields}/>
            </form>

        </div>
        </>
    }
}