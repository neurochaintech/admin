import MainComponent from "../MainComponent";

import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";

export default class Password extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            name: "",
            label: "",
            showPassword: false
        }
        this.state = {...this.state, ...privateProps};
    }

    togglePassword (event, id) {  
        event.preventDefault();
        
        const div = document.getElementById(`password_${this.state.name}`);
        if(div.type === 'password') {
            div.type = 'text';
        } else {
            div.type = 'password';
        }

        super.updateState({ inputs: {"showPassword": !this.state.showPassword} });
    }

    render() {
        return <>
            <label>{this.state.label}</label>
            <button onClick={event => this.togglePassword(event)} type="button" className="button">
                { this.state.showPassword ?
                    <AiFillEye />
                :
                    <AiFillEyeInvisible />
                }
            </button>
            
            <input id={`password_${this.state.name}`} type="password" name={this.state.name} placeholder={this.state.label}/>
        </>
    }
}