import MainComponent from "../MainComponent";
import Password from "./Password";

export default class InputsGenerator extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            fields: []
        }
        this.state = {...this.state, ...privateProps};
    }

    generateField({ field }) {
        if( field.type === "submit" ){
            return(
                <>
                    <input type={field.type} value={field.label} />
                </>
            );
        } else if( field.type === "select" ) {
            return(
                <div className={`col-3`}>
                    <label>{field.label}</label>
                    <select name={field.name}>
                        {field.inputs.map((input, index) => (
                            <option value={input.value} key={`selectField_${field.name}_${index}`}>{input.label}</option>
                        ))}
                    </select>
                </div>
            );
        } else if( field.type === "number" || field.type === "range" ) {
            return(
                <div className={`col-3`}>
                    <label>{field.label}</label>
                    <input 
                        name={field.name}
                        type={field.type} 
                        min={field.min}
                        max={field.max}
                    />
                </div>
            );
        } else if( field.type === "radio" || field.type === "checkbox" ) {
            return(
                <div className={`col-3`}>
                    <label>{field.label}</label>
                    {field.inputs.map((input, index) => (
                        <div className="subInput" key={`selectField_${field.name}_${index}`}>
                            <input 
                                name={field.name}
                                type={field.type}
                                value={input.value}
                            />
                            <label>{input.label}</label>
                        </div>
                    ))}
                </div>
            );
        } else if( field.type === "password" ) {
            return(
                <div className={`col-12`}>
                    <Password name={field.name} label={field.label}/>
                </div>
            );
        } else {
            let colClass = 12;
            if(field.type === "date") { colClass = 3; }

            return(
                <div className={`col-${colClass}`}>
                    <label>{field.label}</label>
                    <input 
                        name={field.name}
                        type={field.type} 
                        placeholder={`${field.label} `}
                    />
                </div>
            );
        }
    }

    render() {
        return <>
            {this.state.fields.map((field, index) => (
                <div className="row" key={`formField_${index}`}>
                    {this.generateField({ field: field })}
                </div>
            ))}
        </>
    }
}