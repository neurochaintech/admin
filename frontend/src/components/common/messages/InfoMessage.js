export default function InfoMessage({ children }) {
    return (
        <div className='alert alert-info'>
            {children}
        </div>
    );
}