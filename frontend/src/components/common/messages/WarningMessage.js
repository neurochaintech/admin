export default function WarningMessage({ children }) {
    return (
        <div className='alert alert-warning'>
            {children}
        </div>
    );
}