export default function DangerMessage({ children }) {
    return (
        <div className='alert alert-danger'>
            {children}
        </div>
    );
}