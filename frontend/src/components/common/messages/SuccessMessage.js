export default function SuccessMessage({ children }) {
    return (
        <div className='alert alert-success'>
            {children}
        </div>
    );
}