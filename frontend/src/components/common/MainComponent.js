import { Component } from 'react';

import DangerMessage from "./messages/DangerMessage";
import InfoMessage from "./messages/InfoMessage";
import SuccessMessage from "./messages/SuccessMessage";
import WarningMessage from "./messages/WarningMessage";

export default class MainComponent extends Component {
    DEFAULT_DANGER_MESSAGE = "Sorry, something went wrong. Please try again latter or contact the administrators.";
    DEFAULT_DATE_OPTIONS = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    }

    constructor() {
        super();

        this.state = {
            danger: "",
            info: "",
            success: "",
            warning: "",
        }
    }

    updateState({ inputs }) {
        let fields = { ...this.state };

        for (const [key, value] of Object.entries(inputs)) {
            if(key in this.state) {
                fields[key] = value;
            }
        };
        this.setState(fields);
    }

    componentDidMount() {
        this.updateState({ inputs: this.props });

        if( this.props.params ) {
            this.updateState({ inputs: this.props.params });
        }
    }

    // #region Render
    
    showMessage({ key, message }) {
        let inputs = {};
        inputs[key] = message;
        this.updateState({ inputs: inputs })
    }

    // handleClick (event, text) {  
    //     event.preventDefault();
        
    //     let fields = { ...this.state };
    //     // fields[e.target.name] =  e.target.value
    //     Object.keys(fields).forEach(function (key) {
    //         fields[key] = fields[key].length === 0 ? text : "";
    //     });

    //     this.setState(fields);
    // }

    render() {
        return <>
            {/* <button className='button' onClick={event => this.handleClick(event, 'Hello world')}>Toggle</button> */}
            
            { this.state.info && this.state.info.length > 0 &&
                <InfoMessage>{this.state.info}</InfoMessage>
            }
            { this.state.success && this.state.success.length > 0 &&
                <SuccessMessage>{this.state.success}</SuccessMessage>
            }
            { this.state.warning && this.state.warning.length > 0 &&
                <WarningMessage>{this.state.warning}</WarningMessage>
            }
            { this.state.danger && this.state.danger.length > 0 &&
                <DangerMessage>{this.state.danger}</DangerMessage>
            }
        </>
    }

    // #endregion Render
}