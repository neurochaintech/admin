import { AiFillCaretDown, AiFillCaretLeft, AiFillCaretUp, AiFillCaretRight, AiOutlineMenu } from "react-icons/ai";

import MainComponent from "../common/MainComponent";

// import SessionTools from "../../tools/SessionTools";

export default class MainSidebar extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            menuDisplayed: window.innerWidth > 600,
            "nodes-accordion": true,
            "nodes-icon": true,
            "blockchain-accordion": true,
            "blockchain-icon": true,
            "admin-accordion": true,
            "admin-icon": true
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        this.synchroniseMainSidebar();
    }

    synchroniseMainSidebar() {
        const mainSidebarDiv = document.getElementById(`mainSidebar`);
        const mainContentDiv = document.getElementById(`mainContent`);
        const toggleDiv = document.getElementById(`mainSidebar-toggle`);

        if( this.state.menuDisplayed ) {
            mainSidebarDiv.removeAttribute('style');
        } else {
            mainSidebarDiv.style.display = "none";
        }
        
        if( this.state.menuDisplayed ) {
            mainContentDiv.removeAttribute('style');
        } else {
            mainContentDiv.style.marginLeft = "0px";
        }

        if( this.state.menuDisplayed ) {
            toggleDiv.removeAttribute('style');
        } else {
            toggleDiv.style.top = "0px";
            toggleDiv.style.left = "0px";
            toggleDiv.style.width = "100%";
            toggleDiv.style.padding = "5px 20px 5px 20px";
            toggleDiv.style.height = "50px";
            const mainSidebarHeaderDiv = document.getElementById(`mainSidebar-header`);
            toggleDiv.style.backgroundColor = window.getComputedStyle(mainSidebarHeaderDiv).backgroundColor;
        }
    }

    toggleMenu(e) {
        e.preventDefault();

        let fields = { ...this.state };
        fields.menuDisplayed = !this.state.menuDisplayed;        
        this.setState(fields);
    }

    toggleAccordion(e, id) {
        e.preventDefault();

        let fields = { ...this.state };

        const accordionDiv = document.getElementById(`${id}-accordion`);
        fields[`${id}-icon`] = accordionDiv.style.display !== "none";
        fields[`${id}-accordion`] = accordionDiv.style.display !== "none";

        this.setState(fields);
    }

    render() {
        return <>
            <div id="mainSidebar">
                <div id="mainSidebar-header">
                    Neurochain
                </div>
                <div id="mainSidebar-content">
                    <nav>
                        <ul>
                            <li onClick={(e) => this.toggleAccordion(e, "nodes")}>
                                <a href="/#">Nodes <i id="nodes-icon">{ this.state["nodes-icon"] ? <AiFillCaretDown /> : <AiFillCaretUp /> }</i></a>
                            </li>
                            <div id="nodes-accordion" className="mainSidebar-content-div" style={ this.state["nodes-accordion"] ? {display:"none"} : {display:"block"}}>
                                <a href="/networks/config">Config</a>
                                <a href="/networks/dockers">Dockers</a>
                                <a href="/networks/status">Status</a>
                                <a href="/networks/peers">Peers</a>
                                <a href="/networks/bots-json">Bot.json</a>
                                <a href="/networks/wallets">Wallets</a>
                                <a href="/networks/logs">Logs</a>
                            </div>

                            <li onClick={(e) => this.toggleAccordion(e, "blockchain")}>
                                <a href="/#">Core <i id="blockchain-icon">{ this.state["blockchain-icon"] ? <AiFillCaretDown /> : <AiFillCaretUp /> }</i></a>
                            </li>
                            <div id="blockchain-accordion" className="mainSidebar-content-div" style={ this.state["blockchain-accordion"] ? {display:"none"} : {display:"block"}}>
                                {/* <a href="/core/blockchain/health">Health</a> */}
                                <a href="/core/blockchain/blocks">Blocks</a>
                                <a href="/core/blockchain/search">Search</a>
                                <a href="/core/blockchain/transactions/sender">Send transactions</a>
                            </div>
                            <li onClick={(e) => this.toggleAccordion(e, "admin")}>
                                <a href="/#">Admin <i id="admin-icon">{ this.state["admin-icon"] ? <AiFillCaretDown /> : <AiFillCaretUp /> }</i></a>
                            </li>
                            <div id="admin-accordion" className="mainSidebar-content-div" style={ this.state["admin-accordion"] ? {display:"none"} : {display:"block"}}>
                                {/* <a href="/admin/api/health">Health</a> */}
                                <a href="/admin/api/cheatsheets">Cheatsheets</a>
                            </div>

                            {/*
                            <li>
                                <form className="search">
                                    <input type="text" className="search-text" placeholder="keywords" />
                                    <button type="submit" className="search-button">
                                        <AiOutlineSearch />
                                    </button>
                                </form>
                            </li> 
                            { SessionTools.isLogged() ?
                                <>
                                    <li onClick={(e) => this.toggleAccordion(e, "logged")}>
                                        <a href="/#">Your space <i id="logged-icon">{ this.state["logged-icon"] ? <AiFillCaretDown /> : <AiFillCaretUp /> }</i></a>
                                    </li>
                                    <div id="logged-accordion" className="mainSidebar-content-div" style={ this.state["logged-accordion"] ? {display:"none"} : {display:"block"}}>
                                        <a href="/">TOTO (TODO)</a>
                                    </div>
                                    <li><a href="/logout">Logout</a></li>
                                </>
                            :
                                <li><a href="/login">Login</a></li>
                            }

                            <li><a href="/">Contact</a></li>
                            */}
                        </ul>
                    </nav>
                </div>
                <div id="mainSidebar-footer">
                    &copy; 2022 Neurochain
                </div>
            </div>

            <div id="mainSidebar-toggle">
                <button id="mainSidebar-toggleButton" onClick={(e) => this.toggleMenu(e)}>
                    { this.state.menuDisplayed &&
                        <AiFillCaretLeft />
                    }
                    <AiOutlineMenu />
                    { !this.state.menuDisplayed &&
                        <AiFillCaretRight />
                    }
                </button>
            </div>
        </>
    }
}