import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

import NodesServices from "../../services/nodes_services";

export default class NodePeers extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            node: {},
            networkId: "",
            data: {}
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 && this.state.data && Object.keys(this.state.data).length === 0 ) {
            this.loadData();
        }
    }

    async loadData() {
        const response = await NodesServices.peers({ networkId: this.state.networkId, nodeId: this.state.node.id });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"data": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        return <>

        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>{this.state.node.id}</h3>
                <p>{this.state.node.hostname}</p>
            </div>
            <div className='card-content'>
                <ReactJson src={this.state.data} name="peers" collapsed={false} theme="rjv-default"/>
            </div>
            <div className='card-footer'>
                {this.state.node.sshHelp}
            </div>
        </div>
        
        {super.render()}
        </>
    }
}