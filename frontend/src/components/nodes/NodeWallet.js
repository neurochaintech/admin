import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

import NodesServices from "../../services/nodes_services";

export default class NodeWallet extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            node: {},
            networkId: "",
            data: {},
            balance: -1
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.data && Object.keys(this.state.data).length === 0 ) {
                this.loadData();
            }
            if( this.state.data.publicKey && this.state.data.publicKey.length > 0 && this.state.balance === -1 ) {
                this.loadBalance();
            }
        }        
    }

    async loadData() {
        const response = await NodesServices.wallet({ networkId: this.state.networkId, nodeId: this.state.node.id });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"data": response.data.row} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadBalance() {
        const response = await NodesServices.balance({ 
            networkId: this.state.networkId, 
            nodeId: this.state.node.id,
            publicKey: this.state.data.publicKey
        });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"balance": response.data.balance} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        return <>

        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>{this.state.node.id}</h3>
                <p>{this.state.node.hostname}</p>
            </div>
            <div className='card-content'>
                <ReactJson src={this.state.data} name="wallet" collapsed={false} theme="rjv-default"/>

                <p className='margin-top-10'>
                    Balance: {new Intl.NumberFormat(process.env.REACT_APP_LOCALE).format(this.state.balance)}
                </p>
            </div>
            <div className='card-footer'>
                {this.state.node.sshHelp}
            </div>
        </div>
        
        {super.render()}
        </>
    }
}