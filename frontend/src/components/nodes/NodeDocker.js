import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

import NodesServices from "../../services/nodes_services";

export default class NodeDocker extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            node: {},
            networkId: "",
            data: {}
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 && this.state.data && Object.keys(this.state.data).length === 0 ) {
            this.loadData(this.props.networkId, this.props.node);
        }
        if(this.props.networkId !== this.state.networkId) {
            super.updateState({ inputs: {
                "data": {},
                "danger": "",
                "networkId": this.props.networkId,
                "node": this.props.node
            } });
        }
    }

    async loadData(networkId, node) {
        const response = await NodesServices.dockers({ networkId: networkId, nodeId: node.id });
        if( response.status === 200 ) {
            super.updateState({ inputs: {
                "data": response.data,
                "networkId": networkId,
                "node": node
            } });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        return <>

        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>{this.state.node.id}</h3>
                <p>{this.state.node.hostname}</p>
            </div>
            <div className='card-content'>
                <ReactJson src={this.state.data} name="dockers" collapsed={false} theme="rjv-default"/>
            </div>
            <div className='card-footer'>
                {this.state.node.sshHelp}
            </div>
        </div>
        
        {super.render()}
        </>
    }
}