import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

import NodesServices from "../../services/nodes_services";

export default class NodeLogs extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            node: {},
            networkId: "",
            data: {},
            lines: 20
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 && this.state.data && Object.keys(this.state.data).length === 0 ) {
            this.loadData(this.state.lines);
        }
    }

    async loadData() {
        const response = await NodesServices.coreLogs({ networkId: this.state.networkId, nodeId: this.state.node.id, lines: this.state.lines });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"data": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    showLines(event, lines) {  
        event.preventDefault();   
        super.updateState({ inputs: {"lines": lines, "data": {}} });
    }

    refresh(event) {  
        event.preventDefault();   
        super.updateState({ inputs: {"data": {}} });
    }

    render() {
        return <>

        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>{this.state.node.id}</h3>
                <p>{this.state.node.hostname}</p>
            </div>
            <div className='card-content'>
                <div className='row'>
                    <div className='col-12'>
                        <button className='button' onClick={event => this.refresh(event)}>Refresh</button>
                        <button className='button' onClick={event => this.showLines(event, 10)}>10</button>
                        <button className='button' onClick={event => this.showLines(event, 20)}>20</button>
                        <button className='button' onClick={event => this.showLines(event, 50)}>50</button>
                    </div>
                </div>

                <ReactJson src={this.state.data} name="logs" collapsed={false} theme="rjv-default"/>
            </div>
            <div className='card-footer'>
                {this.state.node.sshHelp}
            </div>
        </div>
        
        {super.render()}
        </>
    }
}