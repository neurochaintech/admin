import MainComponent from "../common/MainComponent";

import SessionTools from "../../tools/SessionTools";

export default class Login extends MainComponent {
    constructor() {
        super();

        SessionTools.clear();
        window.location.href = `/login`;
    }
}