import { Route } from "react-router-dom";

import Login from "./Login";
import Logout from "./Logout";

const LoginRouter = ( 
    <Route path="/">
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
    </Route>
);
export default LoginRouter;