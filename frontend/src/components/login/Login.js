import MainComponent from "../common/MainComponent";
import InputsGenerator from "../common/forms/InputsGenerator";

import AccountsServices from "../../services/accounts_services";

import LoginTools from "../../tools/LoginTools";
import SessionTools from "../../tools/SessionTools";

export default class Login extends MainComponent {
    loginFields = [
        { label:'Email address', name: 'emailAddress', type: 'email' },
        { label:'Password', name: 'password', type: 'password'  },
        { label:'Log in', type: 'submit'  },
    ]

    // constructor() {
    //     super();
    // }

    async onSubmit(event, fields) {
        event.preventDefault()

        let dangerMessage = "";
        let inputs = {};
        for( const field of fields) {
            if( field.type !== "submit" ) {
                inputs[field.name] = event.target.elements[field.name].value;

                if( field.name === "emailAddress" 
                    && !LoginTools.isEmailAddressValid({ emailAddress: inputs[field.name] }) ) {
                    dangerMessage += `${field.label} is invalid. `;
                }
                if( field.name === "password" 
                    && !LoginTools.isPasswordValid({ password: inputs[field.name] }) ) {
                    dangerMessage += `${field.label} is invalid. `;
                }
            }
        }
        this.updateState({ inputs: { danger: dangerMessage } });

        if( dangerMessage.length === 0 ) {
            const response = await AccountsServices.login({ data: inputs });
            if( response.status === 200 ) {
                SessionTools.saveObject({ key: 'account', data: response.data });
                window.location.href = `/news`;
            } else {
                super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
            }
        }
    }

    render() {
        return <>
            <h1>Login</h1>

            <div onSubmit={event => this.onSubmit(event, this.loginFields)} className="margin-top-30">
                <form className="form">
                    <InputsGenerator fields={this.loginFields}/>

                    {super.render()}
                </form>
            </div>
        </>
    }
}