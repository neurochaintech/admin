import MainComponent from "../common/MainComponent";

import NetworksServices from "../../services/networks_services";
import NodesServices from "../../services/nodes_services";
import TransactionsServices from "../../services/transactions_services";

export default class TransactionsSender extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networks: [],
            nodes: [],
            selectedNetworkId: "",
            selectedNodeId: "",

            // authorPublicKey: "AyMaQAIQcDZbKm6oWwVtKL8shaLQHYrW5R9j2xSlmNGb",
            // authorPrivateKey: "a7be67d160444ba5f8dd646177f6d961dee1bfea91e96937f0fc4ccc27d3ee3ch",
            // recipientPublicKey: "A4xHIKiaMkfqFA8OPXGg2J2bozN5btYPf8iUyQjjVAsJ",
            authorPublicKey: "",
            authorPrivateKey: "",
            recipientPublicKey: "   ",
            value: 1,
            data: "Transaction from admin's console",
            fee: 1,

            authorBalance: -1,
            recipientBalance: -1,
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.networks.length === 0 ) {
                this.loadNetworks();
            }
            if( this.state.selectedNetworkId.length > 0 && this.state.nodes.length === 0 ) {
                this.loadNodes();
            }

            if ( this.state.selectedNetworkId.length > 0 && this.state.selectedNodeId.length > 0 ) {
                if( this.state.authorPublicKey.length > 0 && this.state.authorBalance === -1 ) {
                    this.loadBalance("authorBalance", this.state.authorPublicKey);
                }
                if( this.state.recipientPublicKey.length > 0 && this.state.recipientBalance === -1 ) {
                    this.loadBalance("recipientBalance", this.state.recipientPublicKey);
                }
            }
        }        
    }

    async loadNetworks() {
        const response = await NetworksServices.list({ parameters: {} });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"networks": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadNodes() {
        const response = await NodesServices.list({ networkId: this.state.selectedNetworkId });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"nodes": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadBalance(name, publicKey) {
        const response = await NodesServices.balance({ 
            networkId: this.state.selectedNetworkId, 
            nodeId: this.state.selectedNodeId,
            publicKey: publicKey
        });
        if( response.status === 200 ) {
            let obj = {};
            obj[name] = response.data.balance;
            super.updateState({ inputs: obj });
        } else {
            // super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    networkSelected(event) {
        event.preventDefault();

        const networkId = event.target.value;
        super.updateState({ inputs: {"selectedNetworkId": networkId, "nodes": []} });
    }

    nodeSelected(event) {
        event.preventDefault();

        const nodeId = event.target.value;
        super.updateState({ inputs: {"selectedNodeId": nodeId} });
    }

    async handleChange(event) {
        event.preventDefault();

        let obj = {};
        obj[event.target.name] = event.target.value;

        if( event.target.name.includes("Key") ) {
            obj[event.target.name] = event.target.value.replaceAll(' ', '');
        }

        if( event.target.name === "authorPublicKey" ) {
            obj.authorBalance = -1;
            this.loadBalance("authorBalance", event.target.value);
        }
        if( event.target.name === "recipientPublicKey" ) {
            obj.recipientBalance = -1;
            this.loadBalance("recipientBalance", event.target.value);
        }
        if( event.target.name === "value" || event.target.name === "fee" ) {
            obj[event.target.name] = Math.min( Math.max( 0, obj[event.target.name] ), this.state.authorBalance );
        }

        super.updateState({ inputs: obj });
    }

    async handleSubmit(event) {
        event.preventDefault();

        const transaction = {
            authorPublicKey: this.state.authorPublicKey,
            authorPrivateKey: this.state.authorPrivateKey,
            recipientPublicKey: this.state.recipientPublicKey,
            value: this.state.value,
            data: this.state.data,
            fee: this.state.fee,
        };

        super.showMessage({ key: "info", message: `Sending transaction...` });        
        const response = await TransactionsServices.send({ 
            networkId: this.state.selectedNetworkId, 
            nodeId: this.state.selectedNodeId,
            transaction: transaction 
        });
        if( response.status === 200 ) {
            super.updateState({ inputs: {
                "info": "",
                "success": `Transaction created: ${response.data.row.transactionData.id.data}`
            } });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }
    

    render() {
        return <>
        <h2>Transactions</h2>

        <div className='row'>
            <div className='col-1 vertical-center label'>
                Network:
            </div>
            <div className='col-5'>
                <form>
                    <select onChange={event => this.networkSelected(event)}>
                        <option value="">Please select one</option>
                        { this.state.networks.length > 0 && this.state.networks.map((network, index) => ( 
                            <option value={network.id} key={`network_${index}`}>
                                {network.id}
                            </option>
                        ))}
                    </select>
                </form>
            </div>

            { this.state.nodes.length > 0 &&
                <>
                    <div className='col-1 vertical-center label'>
                        Node:
                    </div>
                    <div className='col-5'>
                        <form>
                            <select onChange={event => this.nodeSelected(event)}>
                                <option value="">Please select one</option>
                                { this.state.nodes.map((node, index) => ( 
                                    <option value={node.id} key={`node_${index}`}>
                                        {node.id}
                                    </option>
                                ))}
                            </select>
                        </form>
                    </div>
                </>
            }
        </div>

        { this.state.selectedNetworkId.length > 0 && this.state.selectedNodeId.length > 0 &&
            <form className="form" onSubmit={event => this.handleSubmit(event)}>
                <h3>Transaction</h3>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Author public key:
                    </div>
                    <div className='col-10'>
                        <input type="text" name="authorPublicKey" value={this.state.authorPublicKey} onChange={event => this.handleChange(event)}/>
                        <p>Balance: {new Intl.NumberFormat(process.env.REACT_APP_LOCALE).format(this.state.authorBalance)}</p>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Author private key exponent:
                    </div>
                    <div className='col-10'>
                        <input type="text" name="authorPrivateKey" value={this.state.authorPrivateKey} onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Recipient public key:
                    </div>
                    <div className='col-10'>
                        <input type="text" name="recipientPublicKey" value={this.state.recipientPublicKey} onChange={event => this.handleChange(event)}/>
                        <p>Balance: {new Intl.NumberFormat(process.env.REACT_APP_LOCALE).format(this.state.recipientBalance)}</p>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Value / Amount:
                    </div>
                    <div className='col-10'>
                        <input type="number" name="value" 
                            min="0"
                            value={this.state.value} 
                            onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Data / Comment:
                    </div>
                    <div className='col-10'>
                        <textarea name="data" 
                            value={this.state.data} 
                            onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-2 vertical-center label'>
                        Fee:
                    </div>
                    <div className='col-10'>
                        <input type="number" name="fee" 
                            min="0"
                            value={this.state.fee} 
                            onChange={event => this.handleChange(event)}/>
                    </div>
                </div>
                <input type="submit" value="Send" />
            </form>
        }
        
        {super.render()}
        </>
    }
}