import { Route } from "react-router-dom";
import BlockchainSearch from "./BlockchainSearch";
import BlocksList from "./BlocksList";
import TransactionsSender from "./TransactionsSender";

const BlockchainRouter = ( 
    <Route path="core/blockchain">
        <Route path="blocks" element={<BlocksList />} />
        <Route path="search" element={<BlockchainSearch />} />
        <Route path="transactions/sender" element={<TransactionsSender />} />
    </Route>
);
export default BlockchainRouter;