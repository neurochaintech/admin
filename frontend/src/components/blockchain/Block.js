import { format } from 'date-fns'
import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

import BlocksServices from "../../services/blocks_services";

export default class Block extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networkId: "",
            nodeId: "",
            blockIndex: -1,
            blockId: "",
            block: {}
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.blockId.length > 0 && Object.keys(this.state.block).length === 0 ) {
                this.loadBlockById();
            }
            if( this.state.blockIndex >= 0 && Object.keys(this.state.block).length === 0 ) {
                this.loadBlockByIndex();
            }
        }
    }

    async loadBlockById() {
        const response = await BlocksServices.getById({ 
            networkId: this.state.networkId,
            nodeId: this.state.nodeId,
            id: this.state.blockId,
        });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"block": response.data.row} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadBlockByIndex() {
        const response = await BlocksServices.loadBlockByIndex({ 
            networkId: this.state.networkId,
            nodeId: this.state.nodeId,
            index: this.state.blockIndex,
        });
        if( response.status === 200 ) {
            let block = response.data.row;
            if(Object.keys(this.state.block).length === 0) {
                block.index = this.state.blockIndex;
            }

            super.updateState({ inputs: {"block": block} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    render() {
        const height = this.state.block.header ? this.state.block.header.height : this.state.blockIndex;
        const encodedId = this.state.block.header ? encodeURIComponent(this.state.block.header.id.data) : "";
        const previousEncodedId = this.state.block.header ? encodeURIComponent(this.state.block.header.previousBlockHash.data) : "";

        return <>
        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>Block {height}: { this.state.block.header ? this.state.block.header.id.data : ""}</h3>                
            </div>
            <div className='card-content'>
                <ReactJson src={this.state.block} name="block" collapsed={false} theme="rjv-default"/>
            </div>
            <div className='card-footer'>
                <p>Timestamp: {this.state.block.header ? format(new Date(this.state.block.header.timestamp.data * 1000), 'dd/MM/yyyy HH:mm:ss') : ""}</p>
                <p>Encoded id: {encodedId}</p>
                <p>Previous encoded id: {previousEncodedId}</p>
            </div>
        </div>
        
        {super.render()}
        </>
    }
}