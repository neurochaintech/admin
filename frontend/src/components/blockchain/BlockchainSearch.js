import MainComponent from "../common/MainComponent";

import NetworksServices from "../../services/networks_services";
import NodesServices from "../../services/nodes_services";
import TransactionsServices from "../../services/transactions_services";
import Transaction from "./Transaction";
import BlocksServices from "../../services/blocks_services";
import Block from "./Block";

export default class BlockchainSearch extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networks: [],
            nodes: [],
            selectedNetworkId: "",
            selectedNodeId: "",

            transactionId: "",
            transactions: [],
            blockId: "",
            blocks: [],
            publicKey: ""
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.networks.length === 0 ) {
                this.loadNetworks();
            }
            if( this.state.selectedNetworkId.length > 0 && this.state.nodes.length === 0 ) {
                this.loadNodes();
            }
        }
    }

    async loadNetworks() {
        const response = await NetworksServices.list({ parameters: {} });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"networks": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadNodes() {
        const response = await NodesServices.list({ networkId: this.state.selectedNetworkId });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"nodes": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    networkSelected(event) {
        event.preventDefault();

        const networkId = event.target.value;
        super.updateState({ inputs: {"selectedNetworkId": networkId, "nodes": [], selectedNodeId: ""} });
    }

    nodeSelected(event) {
        event.preventDefault();

        const nodeId = event.target.value;
        super.updateState({ inputs: {"selectedNodeId": nodeId} });
    }

    async handleChange(event) {
        event.preventDefault();

        let obj = {};
        obj[event.target.name] = event.target.value;

        if( event.target.name.includes("Id") || event.target.name.includes("Key") ) {
            obj[event.target.name] = event.target.value.replaceAll(' ', '');
        }

        super.updateState({ inputs: obj });
    }

    async readTransaction(event) {
        event.preventDefault();

        let transactions = [];
        
        if( this.state.transactionId.length > 0 ) {
            const response = await TransactionsServices.loadById({ networkId: this.state.selectedNetworkId, nodeId: this.state.selectedNodeId, id: this.state.transactionId });
            if( response.status === 200 ) {
                transactions.push( response.data.row ) 
            } else {
                super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
            }
        }
        if( this.state.publicKey.length > 0 ) {
            const responseAuthor = await TransactionsServices.listAuthor({ networkId: this.state.selectedNetworkId, nodeId: this.state.selectedNodeId, publicKey: this.state.publicKey });
            console.log(responseAuthor.data)
            if( responseAuthor.status === 200 ) {
                transactions.push(...responseAuthor.data.rows);
            } else {
                super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
            }

            const responseRecipient = await TransactionsServices.listRecipient({ networkId: this.state.selectedNetworkId, nodeId: this.state.selectedNodeId, publicKey: this.state.publicKey });
            if( responseRecipient.status === 200 ) {
                transactions.push(...responseRecipient.data.rows);
            } else {
                super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
            }
        }

        super.updateState({ inputs: {"transactions": transactions} });
    }

    showTransactions() {
        let outputs = [];
        for(let iTransaction = 0; iTransaction < this.state.transactions.length; ++iTransaction) {
            const transaction = this.state.transactions[iTransaction];
            outputs.push(
                <div key={`tx_${iTransaction}`}  className='row margin-top-20'>
                    <div className='col-12'>
                        <Transaction transaction={transaction}/>
                    </div>
                </div>
            );
        }
        return outputs;
    }

    async readBlock(event) {
        event.preventDefault();

        let blocks = [];
        
        if( this.state.blockId.length > 0 ) {
            const response = await BlocksServices.loadById({ networkId: this.state.selectedNetworkId, nodeId: this.state.selectedNodeId, id: this.state.blockId });
            if( response.status === 200 ) {
                blocks.push( response.data.row )
            } else {
                super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
            }
        }

        super.updateState({ inputs: {"blocks": blocks} });
    }

    showBlocks() {
        let outputs = [];
        for(let iBlock = 0; iBlock < this.state.blocks.length; ++iBlock) {
            const block = this.state.blocks[iBlock];
            outputs.push(
                <div key={`block_${iBlock}`}  className='row margin-top-20'>
                    <div className='col-12'>
                        <Block block={block}/>
                    </div>
                </div>
            );
        }
        return outputs;
    }

    render() {

        return <>
            <h2>Search</h2>

            <div className='row'>
                <div className='col-1 vertical-center label'>
                    Network:
                </div>
                <div className='col-5'>
                    <form>
                        <select onChange={event => this.networkSelected(event)}>
                            <option value="">Please select one</option>
                            { this.state.networks.length > 0 && this.state.networks.map((network, index) => ( 
                                <option value={network.id} key={`network_${index}`}>
                                    {network.id}
                                </option>
                            ))}
                        </select>
                    </form>
                </div>

                { this.state.nodes.length > 0 &&
                    <>
                        <div className='col-1 vertical-center label'>
                            Node:
                        </div>
                        <div className='col-5'>
                            <form>
                                <select onChange={event => this.nodeSelected(event)}>
                                    <option value="">Please select one</option>
                                    { this.state.nodes.map((node, index) => ( 
                                        <option value={node.id} key={`node_${index}`}>
                                            {node.id}
                                        </option>
                                    ))}
                                </select>
                            </form>
                        </div>
                    </>
                }
            </div>

            { this.state.selectedNetworkId.length > 0 
                && this.state.selectedNodeId.length > 0 
                &&
                <>
                    <form className="form" onSubmit={event => this.readBlock(event)}>
                        <h3>Block</h3>
                        <div className="row">
                            <div className='col-2 vertical-center label'>
                                ID / Hash:
                            </div>
                            <div className='col-10'>
                                <input type="text" name="blockId" value={this.state.blockId} onChange={event => this.handleChange(event)}/>
                            </div>
                        </div>
                        <input type="submit" value="Search" />
                    </form>

                    <form className="form margin-top-20" onSubmit={event => this.readTransaction(event)}>
                        <h3>Transaction</h3>
                        <div className="row">
                            <div className='col-2 vertical-center label'>
                                ID / Hash:
                            </div>
                            <div className='col-10'>
                                <input type="text" name="transactionId" value={this.state.transactionId} onChange={event => this.handleChange(event)}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className='col-2 vertical-center label'>
                                Public key:
                            </div>
                            <div className='col-10'>
                                <input type="text" name="publicKey" value={this.state.publicKey} onChange={event => this.handleChange(event)}/>
                            </div>
                        </div>
                        <input type="submit" value="Search" />
                    </form>
                </>
            }

            { this.state.blocks.length > 0 && 
                <>
                    <h3>Blocks</h3>
                    {this.showBlocks()}
                </>
            }

            { this.state.transactions.length > 0 && 
                <>
                    <h3>Transactions</h3>
                    {this.showTransactions()}
                </>
            }
            
            {super.render()}
        </>
    }
}