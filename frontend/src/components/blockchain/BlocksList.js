import MainComponent from "../common/MainComponent";

import NetworksServices from "../../services/networks_services";
import NodesServices from "../../services/nodes_services";
import BlocksServices from "../../services/blocks_services";
import Block from "./Block";
import TransactionsServices from "../../services/transactions_services";

export default class BlocksList extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            networks: [],
            nodes: [],
            selectedNetworkId: "",
            selectedNodeId: "",
            blocksCount: 0,
            transactionsCount: 0,
            page: 0,
            elementsPerPage: 10
        }
        this.state = {...this.state, ...privateProps};
    }

    componentDidUpdate() {
        if( this.state.danger.length === 0 ) {
            if( this.state.networks.length === 0 ) {
                this.loadNetworks();
            }
            if( this.state.selectedNetworkId.length > 0 && this.state.nodes.length === 0 ) {
                this.loadNodes();
            }
            if( this.state.selectedNetworkId.length > 0 
                && this.state.selectedNodeId.length > 0 
                && this.state.blocksCount === 0 ) {
                this.loadBlocksCount();
                this.loadTransactionsCount();
            }
        }
    }

    async loadNetworks() {
        const response = await NetworksServices.list({ parameters: {} });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"networks": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadNodes() {
        const response = await NodesServices.list({ networkId: this.state.selectedNetworkId });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"nodes": response.data.rows} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadBlocksCount() {
        const response = await BlocksServices.count({ 
            networkId: this.state.selectedNetworkId,
            nodeId: this.state.selectedNodeId
        });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"blocksCount": response.data.count} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    async loadTransactionsCount() {
        const response = await TransactionsServices.count({ 
            networkId: this.state.selectedNetworkId,
            nodeId: this.state.selectedNodeId
        });
        if( response.status === 200 ) {
            super.updateState({ inputs: {"transactionsCount": response.data.count} });
        } else {
            super.showMessage({ key: "danger", message: this.DEFAULT_DANGER_MESSAGE })
        }
    }

    networkSelected(event) {
        event.preventDefault();

        const networkId = event.target.value;
        super.updateState({ inputs: {"selectedNetworkId": networkId, "nodes": []} });
    }

    nodeSelected(event) {
        event.preventDefault();

        const nodeId = event.target.value;
        super.updateState({ inputs: {"selectedNodeId": nodeId} });
    }

    showBlocks(iBegin, iEnd) {
        let outputs = [];
        for(let iBlock = iBegin; iBlock >= iEnd; --iBlock) {
            outputs.push(
                <div key={iBlock}  className='row margin-top-20'>
                    <div className='col-12'>
                    <Block blockIndex={iBlock} networkId={this.state.selectedNetworkId} nodeId={this.state.selectedNodeId}/>
                    </div>
                </div>
            );
        }
        return outputs;
    }

    showPage(event, increment) {  
        event.preventDefault();

        super.updateState({ inputs: {"page": this.state.page+increment} });
    }

    render() {
        let iBegin = this.state.blocksCount - (this.state.page * this.state.elementsPerPage);
        let iEnd = this.state.blocksCount - ((this.state.page+1) * this.state.elementsPerPage);

        iBegin = Math.max( Math.min(this.state.blocksCount, iBegin) -1, 0 );
        iEnd = Math.max(0, iEnd);

        return <>
            <h3>Settings</h3>

            <div className='row'>
                <div className='col-1 vertical-center label'>
                    Network:
                </div>
                <div className='col-5'>
                    <form>
                        <select onChange={event => this.networkSelected(event)}>
                            <option value="">Please select one</option>
                            { this.state.networks.length > 0 && this.state.networks.map((network, index) => ( 
                                <option value={network.id} key={`network_${index}`}>
                                    {network.id}
                                </option>
                            ))}
                        </select>
                    </form>
                </div>

                { this.state.nodes.length > 0 &&
                    <>
                        <div className='col-1 vertical-center label'>
                            Node:
                        </div>
                        <div className='col-5'>
                            <form>
                                <select onChange={event => this.nodeSelected(event)}>
                                    <option value="">Please select one</option>
                                    { this.state.nodes.map((node, index) => ( 
                                        <option value={node.id} key={`node_${index}`}>
                                            {node.id}
                                        </option>
                                    ))}
                                </select>
                            </form>
                        </div>
                    </>
                }
            </div>

            { this.state.blocksCount > 0 &&
                <>
                    <h3>Blocks: {iBegin} - {iEnd} / { Math.max(this.state.blocksCount-1, 0) }</h3>
                    <h3>Transactions: {this.state.transactionsCount}</h3>

                    <div className="row">
                        <div className="col-12 text-center">
                            { iBegin < this.state.blocksCount-1 &&
                                <button className='button' onClick={event => this.showPage(event, -1)}>Previous</button>
                            }
                            { iEnd > 0 &&
                                <button className='button' onClick={event => this.showPage(event, +1)}>Next</button>
                            }
                        </div>
                    </div>            

                    {this.showBlocks(iBegin, iEnd)}
                </>
            }            

            {super.render()}
        </>
    }
}