import ReactJson from 'react-json-view'

import MainComponent from "../common/MainComponent";

// import TransactionsServices from '../../services/transactions_services';

export default class Transaction extends MainComponent {
    constructor() {
        super();

        const privateProps = {
            transaction: {}
        }
        this.state = {...this.state, ...privateProps};
    }

    render() {
        return <>
        <div className='card margin-bottom-20'>
            <div className='card-header'>
                <h3>Transaction: { this.state.transaction.id ? this.state.transaction.id.data : ""}</h3>
            </div>
            <div className='card-content'>
                <ReactJson src={this.state.transaction} name="transaction" collapsed={false} theme="rjv-default"/>
            </div>
            <div className='card-footer'>
                <p>Encoded id: {this.state.transaction.id ? encodeURIComponent(this.state.transaction.id.data) : ""}</p>
                <p>Block: </p>
                <p>
                    Previous block:&nbsp; 
                    { this.state.transaction.lastSeenBlockId && 
                        <a href={`/blockchain/blocks/${encodeURIComponent(this.state.transaction.lastSeenBlockId.data)}`}>{encodeURIComponent(this.state.transaction.lastSeenBlockId.data)} (TODO)</a>
                    }
                </p>
            </div>
        </div>
        
        {super.render()}
        </>
    }
}