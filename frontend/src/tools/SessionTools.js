export default class SessionTools {
    static isLogged() {
        let logged = false;
        
        if( sessionStorage.account ) {
            const session = JSON.parse(sessionStorage.account);
            // TODO: secure check
            logged = session.secretToken && session.secretToken.length === 64;
            // logged &= ;
        }

        return logged;
    }

    static saveObject({ key, data }) {
        sessionStorage.setItem(key, JSON.stringify(data));
    }

    static getObject({ key }) {
        let output = {};

        if( sessionStorage[key] ) {
            output = JSON.parse(sessionStorage[key]);
        }

        return output;
    }
    
    static clear() {
        sessionStorage.clear();
    }
}
