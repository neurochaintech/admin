import TextTools from "./TextTools";

export default class LoginTools {
    static lowercaseCharacters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    static uppercaseCharacters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    static numberCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    static specialCharacters = ["+", "-", "&", "|", "!", "(", ")", "{", "}", "[", "]", "^","~", "*", "?", ":"];

    static isPseudoValid({ pseudo }) {
        return typeof pseudo !== "undefined"
            && pseudo.length >= 5
        ;
    }

    static isPasswordValid({ password }) {

        const valid = password.length >= 8
            && TextTools.substringsInString({ string: password, array: this.numberCharacters })
            && TextTools.substringsInString({ string: password, array: this.specialCharacters })
            && TextTools.substringsInString({ string: password, array: this.lowercaseCharacters })
            && TextTools.substringsInString({ string: password, array: this.uppercaseCharacters })

        return valid;
    }

    // RFC 2822
    static isEmailAddressValid({ emailAddress = "" }) {
        // eslint-disable-next-line
        const mailformat = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
        
        return typeof emailAddress !== "undefined"
            && mailformat.test(emailAddress)
        ;
    }
}