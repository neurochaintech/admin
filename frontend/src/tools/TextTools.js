export default class TextTools {

    static substringsInString({ string, array }){
        return array.some(substring => {
            if (string.includes(substring)) {
              return true;
            }
          
            return false;
        });
    }

    static capitalizeFirstLetter({ string }) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }  
}