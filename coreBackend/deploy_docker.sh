#!/bin/bash

set -e

CONTAINER_NAME="core-backend"
CONTAINER_ID=$( docker ps -aqf "name=$CONTAINER_NAME" )

if [ "$CONTAINER_ID" = "" ]; then
    echo "$CONTAINER_NAME does not exist."
else
    echo "$CONTAINER_NAME found."
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

docker build . -t neurochain/${CONTAINER_NAME}

docker run --name $CONTAINER_NAME --network="neurochain" -p 5822:5822 -d neurochain/${CONTAINER_NAME}

docker container ls --all
# docker logs --follow $CONTAINER_NAME