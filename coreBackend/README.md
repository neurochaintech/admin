# Backend

## Install

```
npm install

npm run importDatabase-dev
```

## Launch

```
npm run start-dev
npm run start-prod
```

## Deploy

```
setup ./admin/deployOnNodes.js
npm run deployOnNodes
```