"use strict";

import { format } from 'date-fns'
import fs from "fs";
import util from "util"

export default class LogsTools {
    static DIR_PATH = `${process.cwd()}/;ogs`;

    static TextColours = {
        Black: "\x1b[30m",
        Red: "\x1b[31m",
        Green: "\x1b[32m",
        Yellow: "\x1b[33m",
        Blue: "\x1b[34m",
        Magenta: "\x1b[35m",
        Cyan: "\x1b[36m",
        White: "\x1b[37m",
    }
    
    static write({ element, textColour }) {
        const date = format(new Date().getTime(), 'dd/MM/yyyy HH:mm:ss');
        const header = `${date}:`;
        
        if( process.env.SHOW_LOGS_CONSOLE === 'true' ) {
            if( textColour ) {
                console.log(`${textColour}${header}`, element, `\x1b[0m`);
            } else {
                console.log(header, util.inspect(element, {showHidden: false, depth: null, colors: true}))
            }
        }
        
        if( process.env.SAVE_LOGS === 'true' ) {
            const date = format(new Date().getTime(), 'yyyyMMdd');
            const filePath = `${this.DIR_PATH}/${date}.log`;
            const writeStream = fs.createWriteStream(filePath, {flags: 'a'});
            const redirectedConsole = new console.Console(writeStream);
            redirectedConsole.log(header, element);
            writeStream.end("");
        }
    }
}