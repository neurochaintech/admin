"use strict";

import * as child from 'child_process';
import LogsTools from './logs_tools.js';

export default class ShellTools {
    static async execute({ command, timeout = 5 * 1000 }) {
        let stdout = "";
        
        if(command) {
            try {
                const rawStdout = child.execSync(command, { timeout: timeout, stdio : 'pipe' });
                stdout = rawStdout.toString();

                LogsTools.write({ element: `ShellTools: ${command}`, textColour: LogsTools.TextColours.Yellow });
            } catch (error) {
                LogsTools.write({ element: `ShellTools: execute ERROR`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: `ShellTools: ${command}`, textColour: LogsTools.TextColours.Red });
                LogsTools.write({ element: error });
            }
        }

        return stdout;
    }
}