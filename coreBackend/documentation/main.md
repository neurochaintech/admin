# API documentation

## List requests

To add a filter field, three elements are needed:
- a logical operator
- a field
- a comparison operator

Optionals:
- retrievedFields: fields name that need to be returned, by default they are all selected (ex: `retrievedFields: "_id index title"`)
- sort: list's order, by default it's from the most recent inserted to the oldest ("insertedDate_-1"). If multiple fields then use the separator ",".
- elementsPerPage: max rows or documents by request, by default 20
- page: by default the first page is 0
- from: index of rows or documents skipped
- to: last index of rows or documents
- count: if the query only wants to count the elements / documents / rows.

Dates format is YYYY-MM-DD or YYYY-MM-DDTHH:MM:SS

## Logical operators

```
and
or
```

## Fields

They are equal to the schemas' fields. Please refer to the appropriate ".md" file.

## Comparison operators

### Text or string
```
like
nlike
eq
ne
in ("," separator)
nin ("," separator)
```

### Numbers or dates
```
eq
gt
gte
lt
lte
ne
```

### booleans
```
eq
```

### IDs
```
eq
ne
```

## Examples

List 50 books with the world "sword", sorted by decroissant release date then in alphabetic order
```
http://localhost:582/api/books?and_title_like=sword&elementsPerPage=50&sort=releaseDate_-1,title_1
```

Books from 50 to 100:
```
http://localhost:582/api/books?and_title_like=sword&elementsPerPage=50&page=1&sort=releaseDate_-1,title_1
```

Books count:
```
http://localhost:582/api/books?and_title_like=sword&count=true
```

## Login and authorization

After being logged in, the elements bellow need to be saved. Afterwards, they need to be added in the request if a routes the user has been logged or if a special authorization is required (admin access for example).
```
{
    accountId
    secretToken
}
```

## Common to all schemas / models
```
updatedDate:    date when element has been updated
insertedDate:   date when element has been created
active:         element's status ("active", "archived", "deleted"), by default "active"
```