# BLOCKS

## Schema or model

```

"block.header.id.data":{type:String},
"block.header.timestamp.data":{type:Number},
"block.header.previousBlockHash.data":{type:String},
"block.header.author.keyPub.rawData":{type:String},
"branchPath.blockNumbers.0":{type:Number},
"branchPath.blockNumber":{type:Number},
"branch":{type:String},

```

## Routes

```
METHOD: GET

URL: http://127.0.0.1:5822/blocks
```
```
METHOD: GET

URL: http://127.0.0.1:5822/blocks/branches
```
```
METHOD: GET

URL: http://127.0.0.1:5822/blocks/:id
```
