# TRANSACTIONS

## Schema or model

```

"transaction.inputs.keyPub.rawData":{type:String},
"transaction.inputs.value.value":{type:String},
"transaction.outputs.keyPub.rawData":{type:String},
"transaction.outputs.value.value":{type:String},
"transaction.lastSeenBlockId.data":{type:String},
"transaction.fees.value":{type:Number},
"blockId.data":{type:String},
"timestamp.data":{type:Number},

```

## Routes

```
METHOD: GET

URL: http://127.0.0.1:5822/transactions
```
```
METHOD: GET

URL: http://127.0.0.1:5822/transactions/:id
```
