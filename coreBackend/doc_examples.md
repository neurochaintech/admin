# Core Backend API

## Examples

All requests are in GET.

### Id

Get ID for API:
```
encodeURIComponent('QPcVgloZww85Mso8X7iKDv+/kSmXB1+L8d4l4BK2dC0=')
> QPcVgloZww85Mso8X7iKDv%2B%2FkSmXB1%2BL8d4l4BK2dC0%3D

encodeURIComponent('ApvvQXz/u74xBw0EGel4dMlum22aggyz1Da8vdzajAU3')
> ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3
```

### Blocks

List blocks from most recent to oldest
```
# With pagination system
http://127.0.0.1:5822/api/blocks?and_branch_eq=MAIN&page=0&elementsPerPage=20&sort=block.header.timestamp.data_-1

# With from to system
http://127.0.0.1:5822/api/blocks?and_branch_eq=MAIN&from=0&to=100&sort=block.header.timestamp.data_-1
```

Count blocks
```
http://127.0.0.1:5822/api/blocks?and_branch_eq=MAIN&count=true
```

Block by id
```
http://127.0.0.1:5822/api/blocks/QPcVgloZww85Mso8X7iKDv%2B%2FkSmXB1%2BL8d4l4BK2dC0%3D
```

### Transactions

List transactions
```
# All
http://127.0.0.1:5822/api/transactions?page=0&elementsPerPage=20&sort=timestamp.data_-1

# Author
http://127.0.0.1:5822/api/transactions?and_transaction.inputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3
http://127.0.0.1:5822/api/transactions?and_transaction.inputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3&count=true

# Receiver
http://127.0.0.1:5822/api/transactions?and_transaction.outputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3
http://127.0.0.1:5822/api/transactions?and_transaction.outputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3&count=true

# Author or receiver
http://127.0.0.1:5822/api/transactions?or_transaction.inputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3&or_transaction.outputs.keyPub.rawData_eq=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3
```

Count transactions
```
http://127.0.0.1:5822/api/transactions?count=true
```

Transactions from block ID
```
http://127.0.0.1:5822/api/transactions?and_blockId.data_eq=QPcVgloZww85Mso8X7iKDv%2B%2FkSmXB1%2BL8d4l4BK2dC0%3D
```

Transaction by id
```
http://127.0.0.1:5822/api/transactions/7p0eGxBKVGA1mDvUbRefbVBwM5ZkjIponCC24nM25Fc%3D
```

### Admins

All blocks from block number 0 included to block number 10 excluded with transactions containing the public key as the author or the receiver:

```
[0-10[
ApvvQXz/u74xBw0EGel4dMlum22aggyz1Da8vdzajAU3
http://127.0.0.1:5822/api/admin/transactions?publicKey=ApvvQXz%2Fu74xBw0EGel4dMlum22aggyz1Da8vdzajAU3&blockNumber_from=0&blockNumber_to=10

[0-10[
http://ec2-35-181-28-116.eu-west-3.compute.amazonaws.com:5822/api/admin/transactions?publicKey=AyMaQAIQcDZbKm6oWwVtKL8shaLQHYrW5R9j2xSlmNGb&&blockNumber_from=0&blockNumber_to=10
```