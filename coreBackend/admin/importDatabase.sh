#!/bin/bash

database_name=$1
input_path=$2
echo "Import database: ${database_name} from ${input_path}"

files=`ls ${input_path}/*.json`
for filePath in $files
do
    if [[ -f "$filePath" ]]
    then
        echo "Import from $filePath"

        IFS="/" read -r -a path_elements <<< "$filePath"
        file=${path_elements[-1]}

        separator="_"
        IFS="$separator" read -r -a name_elements <<< "$file"
        unset 'name_elements[${#name_elements[@]}-1]'
        collectionName="$( printf "${separator}%s" "${name_elements[@]}" )"
        collectionName="${collectionName:${#separator}}" # remove leading separator

        echo "Import collection: ${collectionName}"
        mongoimport --db "$database_name" --collection "$collectionName" --file "$filePath"
    fi
done