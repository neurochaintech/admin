"use strict";

import FileSystemTools from "../tools/fileSystem_tools.js";

async function main() {
    try {
        const mainURL = `http://127.0.0.1:${process.env.PORT}`;

        const apiPath = `${process.cwd()}/api`;
        const routersPaths = FileSystemTools.getFilesPaths({ inputPath: apiPath, recursive: true, whitelist: ["_router.js"]  });
        for(const routerPath of routersPaths) {
            const pathElements = routerPath.split('/');

            const name = pathElements[pathElements.length-2];
            const outputFilePath = `${process.cwd()}/documentation/${name}.md`;

            console.log(`Clean up: ${outputFilePath}`);
            FileSystemTools.deleteFile({ inputPath: outputFilePath })

            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: `# ${name.toUpperCase()}\n\n` 
            });


            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: `## Schema or model\n\n` 
            });
            const daoPath = routerPath.replace('_router.js', '_dao.js');
            const daoContent = FileSystemTools.readFile({ inputPath: daoPath });
            
            const startPattern = 'static SCHEMA = {';
            const indexStart = daoContent.indexOf(startPattern);
            const endPattern = '};';
            const indexEnd = daoContent.indexOf(endPattern);
            let schema = daoContent.substring(indexStart + startPattern.length, indexEnd);
            schema = schema.replaceAll(" ", "");

            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: `\`\`\`\n`
            });
            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: schema
            });
            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: `\n\`\`\`\n\n`
            });
            
            const enumPath = routerPath.replace('_router.js', '_enums.js');
            if( FileSystemTools.doesExist({ inputPath: enumPath }) ) {
                const enums = await import( enumPath );
                FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                    data: `## Enums\n\n` 
                });

                for( const [key, value] of Object.entries(enums.default) ) {
                    FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                        data: `### ${key}\n\n` 
                    });
                    FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                        data: `\`\`\`\n`
                    });
                    FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                        data: JSON.stringify(value, null, 4)
                    });
                    FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                        data: `\n\`\`\`\n\n`
                    });
                }
            }


            FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                data: `## Routes\n\n` 
            });
            const apiRouter = await import(routerPath);
            for( const layer of apiRouter.default.stack ) {
                for( const method of Object.keys(layer.route.methods) ) {
                    FileSystemTools.appendToFile({ inputPath: outputFilePath, 
                        data: `\`\`\`
METHOD: ${method.toUpperCase()}\n
URL: ${mainURL}${layer.route.path}
\`\`\`\n` 
                    });
                }
            }

            console.log(`Generated: ${outputFilePath}`);
        }
        
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};
main();