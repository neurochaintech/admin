#!/bin/bash

database_name=$1
echo "Exporting database: ${database_name}"

current_date=$(date '+%Y%m%d')
output_dir_path="./outputs/${database_name}_${current_date}"
mkdir -p ${output_dir_path}

collections_mongo=$( mongo --quiet --eval 'db.getCollectionNames()' ${database_name} )
collections_clean_string=$( echo "${collections_mongo//[[\]\"]}" )

IFS=', ' read -r -a collections_array <<< ${collections_clean_string}
for (( i = 0; i < ${#collections_array[@]}; ++i )); do
    echo "Exporting collection: ${collections_array[i]}"

    output_path="${output_dir_path}/${collections_array[i]}_${current_date}.json"
    mongoexport --db ${database_name} --collection ${collections_array[i]} --out "${output_path}"
done
