"use strict";

import dotenv from "dotenv";
import { exec } from "child_process";

async function main() {
    try {
        const envFile = `.env.${process.env.NODE_ENV}`;
        dotenv.config({ path: envFile });
        const inputPath = `${process.cwd()}/admin/testnet_20220325`;
        
        console.log(`Import database: ${process.env.DATABASE_NAME} from ${inputPath}`);
        exec(`${process.cwd()}/admin/importDatabase.sh "${process.env.DATABASE_NAME}" "${inputPath}"`, function(error, stdout, stderr) {
            if(error) { console.log("error:", error); }
            if(stdout) { console.log("stdout:", stdout); }
            if(stderr) { console.log("stderr:", stderr); }
        });
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};
main();