"use strict";

// import dotenv from "dotenv";
import FileSystemTools from "../tools/fileSystem_tools.js";

async function generateAPI() {
    try {
        const apiPath = `${process.cwd()}/api`;
        const dirPath = `${apiPath}/${nameLowerCase}`;
        FileSystemTools.deleteDirectory({ inputPath: dirPath });

        FileSystemTools.createDirectory({ inputPath: dirPath });

        FileSystemTools.appendToFile({ inputPath: `${dirPath}/${nameLowerCase}_controller.js`, 
            data: `
"use strict";

import Controller from "../controller.js";

import ${name}DAO from "./${nameLowerCase}_dao.js";

export default class ${name}Controller extends Controller {
    static dao = ${name}DAO;
}` 
        });

        FileSystemTools.appendToFile({ inputPath: `${dirPath}/${nameLowerCase}_dao.js`, 
            data: `
"use strict";

import mongoose from "mongoose";

import MongodbConstants from "../mongodb_constants.js";
import MongodbDAO from "../mongodb_dao.js";

export default class ${name}DAO extends MongodbDAO {
    static COLLECTION_NAME = "${nameLowerCase}";
    static SCHEMA = {


        updatedDate:            { type: Date, default: Date.now },
        insertedDate:           { type: Date, default: Date.now },
        active:                 { type: String, enum: Object.values(MongodbConstants.ActiveEnum), default: MongodbConstants.ActiveEnum.Active },
    };
}` 
        });

        FileSystemTools.appendToFile({ inputPath: `${dirPath}/${nameLowerCase}_router.js`, 
            data: `
"use strict";

import { Router } from "express";

import ${name}Controller from "./${nameLowerCase}_controller.js";

const router = new Router();

router
    .route("/${nameLowerCase}")
    .post( (req, res, next ) => ${name}Controller.create(req, res, next) )
    .get( (req, res, next ) => ${name}Controller.list(req, res, next) )
;

router
    .route("/${nameLowerCase}/:id")
    .get( (req, res, next ) => ${name}Controller.read(req, res, next) )
    .put( (req, res, next ) => ${name}Controller.update(req, res, next) )
    .delete( (req, res, next ) => ${name}Controller.delete(req, res, next) )
;

export default router;` 
        });
    } catch(e) {
        console.error(e);
        process.exit(2);
    }
}

async function generateService() {
    try {
        const apiPath = `${process.cwd()}/services`;
        const filePath = `${apiPath}/${nameLowerCase}_services.js`;

        FileSystemTools.deleteFile({ inputPath: filePath });

        FileSystemTools.appendToFile({ inputPath: filePath, 
            data: `import Services from "./services.js";

export default class ${name}Services extends Services {
    static COLLECTION_NAME = "${nameLowerCase}";
}` 
        });
    } catch(e) {
        console.error(e);
        process.exit(3);
    }
}

async function main() {
    try {
        await generateAPI();
        await generateService();
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};

const name = process.argv[2];
const nameLowerCase = name.toLowerCase();
console.log(`Generate model: ${name}`)

main();
