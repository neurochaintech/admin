"use strict";

import mongoose from "mongoose"

import LogsTools from "./tools/logs_tools.js"
import FileSystemTools from "./tools/fileSystem_tools.js"

import Backend from "./backend.js";

async function connectDatabase() {
    try {
        LogsTools.write({ element: `Connect to database: BEGIN`, textColour: LogsTools.TextColours.Blue });

        const connection = await mongoose.connect(`${process.env.DATABASE_URI}/${process.env.DATABASE_NAME}`, { 
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        
        // #region DAO

        const apiPath = `${process.cwd()}/api`;
        const modelsPaths = FileSystemTools.getFilesPaths({ inputPath: apiPath, recursive: true, whitelist: ["_dao.js"]  });
        
        let modelsConnections = [];
        for(const modelPath of modelsPaths) {
            const promise = new Promise(async (resolve, reject) => {
                const model = await import(modelPath);
                if( !modelPath.includes("mongodb_dao") ) {
                    await model.default.injectDB({ connection: connection });
                } 
                resolve();
            });
            modelsConnections.push(promise);
        }
        await Promise.all(modelsConnections);
        
        // #endregion DAO

        LogsTools.write({ element: `Connect to database: END`, textColour: LogsTools.TextColours.Blue });
        
    } catch(e) {
        LogsTools.write({ element: e, textColour: LogsTools.TextColours.Red  });
    }
}

async function launchServer() {
    try {
        await connectDatabase();
        
        const server = Backend.listen(process.env.PORT || 3001, function () {
            const address = server.address().address === "::" ? "127.0.0.1" : server.address().address;
            LogsTools.write({ element: `Backend listening on: http://${address}:${server.address().port}`, textColour: LogsTools.TextColours.Green });
        });
    } catch(e) {
        LogsTools.write({ element: e, textColour: LogsTools.TextColours.Red  });
        process.exit(1);
    }
};
launchServer();