
"use strict";

// import mongoose from "mongoose";
import LogsTools from "../../tools/logs_tools.js";
import MongodbODM from "../mongodb_odm.js";

// import MongodbConstants from "../mongodb_constants.js";
import MongodbDAO from "../mongodb_dao.js";
// import TransactionsDAO from "../transactions/transactions_dao.js";

export default class BlocksDAO extends MongodbDAO {
    static COLLECTION_NAME = "blocks";
    static SCHEMA = {
        "block.header.id.data": { type: String },
        "block.header.timestamp.data": { type: Number },
        "block.header.previousBlockHash.data": { type: String },
        "block.header.author.keyPub.rawData": { type: String },
        "branchPath.blockNumbers.0": { type: Number },
        "branchPath.blockNumber": { type: Number },
        "branch": { type: String },
    };

    // static async list({ inputs }) {
    //     let list = [];

    //     const elementsPerPage = inputs.elementsPerPage ? parseInt(inputs.elementsPerPage) : 20;
    //     const page = inputs.page ? parseInt(inputs.page) : 0;
    //     const skip = inputs.from ? inputs.from : elementsPerPage * page;
    //     const limit = inputs.to ? inputs.from - inputs.to : elementsPerPage;

    //     const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
    //     const sortOptions = MongodbODM.formatSort({ parameters: inputs, modelFields: this.model.schema.paths });
        
    //     const retrievedFields = inputs.retrievedFields ? inputs.retrievedFields : "-__v";

    //     LogsTools.write({ element: `${this.name}: LIST` });
    //     LogsTools.write({ element: filters });
    //     LogsTools.write({ element: sortOptions });
    //     LogsTools.write({ element: `elementsPerPage:${elementsPerPage} page:${page}` });

    //     try {
    //         if( inputs.count ) {
    //             const count = await this.model.countDocuments(filters);
    //             list.push({count: count});
    //         } else {
    //             const listCursors = await this.model.find(filters, retrievedFields)
    //                 .limit(limit)
    //                 .skip(skip)
    //                 .sort(sortOptions)
    //                 .exec();

    //             for(let iRow = 0; iRow < listCursors.length; ++iRow) {
    //                 let row = listCursors[iRow].toObject({ versionKey: false });
    //                 MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });
    //                 list.push(row);
    //             }

    //             // const listCursors = await this.model.aggregate([
    //             //     { $match: filters }
    //             // ])
    //             // .limit(limit)
    //             // .skip(skip)
    //             // .sort(sortOptions)
    //             // .lookup({ from: 'transactions', localField: 'block.header.id.data', foreignField: 'blockId.data', as: 'transactions' })
    //             // .project({ block: {
    //             //         $filter: {
    //             //             input: "$transactions",
    //             //             as: "transactionRow",
    //             //             cond: { 
    //             //                 $eq: [ "$$transactionRow.transaction.inputs.keyPub.rawData", "ApvvQXz/u74xBw0EGel4dMlum22aggyz1Da8vdzajAU3" ]
    //             //             }
    //             //         }
    //             //     }})
    //             // ;
    //             // list = listCursors;
    //         }
    //     } catch(errorList) {
    //         LogsTools.write({ element: `${this.name}: LIST ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: inputs });
    //         LogsTools.write({ element: errorList });
    //     }

    //     return list;
    // }

    // static async findOne({ inputs }) {
    //     const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
    //     const retrievedFields = inputs.retrievedFields ? inputs.retrievedFields : "-__v";

    //     try {
    //         let row = (await this.model.findOne(filters, retrievedFields).exec()).toObject({ versionKey: false });
    //         MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });

    //         LogsTools.write({ element: `${this.name}: FINDONE`, textColour: LogsTools.TextColours.Green });
    //         LogsTools.write({ element: inputs });
            
    //         row.transactions = await TransactionsDAO.list({ inputs: {
    //             "and_blockId.data_eq": row.block.header.id.data,
    //             elementsPerPage: 50,
    //             page: 0,
    //         } });

    //         return row;
    //     } catch(error) {
    //         LogsTools.write({ element: `${this.name}: FINDONE ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: inputs });
    //         return { error: error };
    //     }
    // }
}