
"use strict";

import Controller from "../controller.js";

import BlocksDAO from "./blocks_dao.js";

export default class BlocksController extends Controller {
    static dao = BlocksDAO;

    static async read(req, res, next) {
        const row = await this.dao.findOne({ inputs: {
            "block.header.id.data": req.params.id
        } });

        if( row.id ) {
            res.status(200).json({ row: row });
        } else {
            res.status(400).json(row);
        }

        next();
    }

    static async distinct(req, res, next) {
        const rows = await this.dao.distinct({ field: "branch" });
        
        let output = [];
        for( const branchName of rows ) {
            const reponse = await this.dao.list({ inputs: {
                and_branch_eq: branchName,
                count: true
            }});

            output.push({
                name: branchName,
                count: reponse[0].count
            });
        }

        if( rows ) {
            res.status(200).json({ rows: output });
        } else {
            res.status(400).json(rows);
        }
    }
}