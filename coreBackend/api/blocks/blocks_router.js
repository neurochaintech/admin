
"use strict";

import { Router } from "express";

import BlocksController from "./blocks_controller.js";

const router = new Router();

router
    .route("/blocks")
    .get( (req, res, next ) => BlocksController.list(req, res, next) )
;

router
    .route("/blocks/branches")
    .get( (req, res, next ) => BlocksController.distinct(req, res, next) )
;

router
    .route("/blocks/:id")
    .get( (req, res, next ) => BlocksController.read(req, res, next) )
;

export default router;