"use strict";

import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

import LogsTools from "../tools/logs_tools.js";
import MongodbODM from "./mongodb_odm.js";

export default class MongodbDAO {
    static COLLECTION_NAME = "tmp";
    static SCHEMA = {};
    static model;
    
    static async injectDB({ connection }) {
        try {
            const mongooseSchema = new mongoose.Schema(this.SCHEMA);
            mongooseSchema.plugin(uniqueValidator);
            this.model = connection.model(this.COLLECTION_NAME, mongooseSchema);
            LogsTools.write({ element: `${this.name}: ${this.COLLECTION_NAME} successful connection`, textColour: LogsTools.TextColours.Green });
        } catch(e) {
            LogsTools.write({ element: `${this.name}: ${this.COLLECTION_NAME} unable to connect`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: e });
        }
    }

    static async list({ inputs, sanitise = true }) {
        let list = [];

        const elementsPerPage = inputs.elementsPerPage ? parseInt(inputs.elementsPerPage) : 20;
        const page = inputs.page ? parseInt(inputs.page) : 0;
        const skip = inputs.from ? inputs.from : elementsPerPage * page;
        const limit = inputs.to ? inputs.from - inputs.to : elementsPerPage;

        const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
        const sortOptions = MongodbODM.formatSort({ parameters: inputs, modelFields: this.model.schema.paths });
        
        const retrievedFields = inputs.retrievedFields ? inputs.retrievedFields : "-__v";

        LogsTools.write({ element: `${this.name}: LIST` });
        LogsTools.write({ element: filters });
        LogsTools.write({ element: sortOptions });
        LogsTools.write({ element: `elementsPerPage:${elementsPerPage} page:${page}` });

        try {
            if( inputs.count ) {
                const count = await this.model.countDocuments(filters);
                list.push({count: count});
            } else {
                const listCursors = await this.model.find(filters, retrievedFields).limit(limit).skip(skip).sort(sortOptions).exec();

                if(sanitise) {
                    for(let iRow = 0; iRow < listCursors.length; ++iRow) {
                        let row = listCursors[iRow].toObject({ versionKey: false });
                        MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });
                        list.push(row);
                    }
                } else {
                    list = listCursors;
                }
            }
        } catch(errorList) {
            LogsTools.write({ element: `${this.name}: LIST ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: inputs });
            LogsTools.write({ element: errorList });
        }

        return list;
    }

    static async findOne({ inputs }) {
        const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
        const retrievedFields = inputs.retrievedFields ? inputs.retrievedFields : "-__v";

        try {
            let row = (await this.model.findOne(filters, retrievedFields).exec()).toObject({ versionKey: false });
            MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });

            LogsTools.write({ element: `${this.name}: FINDONE`, textColour: LogsTools.TextColours.Green });
            LogsTools.write({ element: inputs });

            return row;
        } catch(error) {
            LogsTools.write({ element: `${this.name}: FINDONE ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: inputs });
            return { error: error };
        }
    }

    static async distinct({ field }) {
        LogsTools.write({ element: `${this.name}: DISTINCT ${field}` });

        let list = [];
        try {
            list = await this.model.find().distinct(field);
        } catch(errorList) {
            LogsTools.write({ element: `${this.name}: DISTINCT ERROR`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: field });
            LogsTools.write({ element: errorList });
        }

        return list;
    }

    // static async create({ inputs }) {
    //     const newDocument = new this.model(inputs);

    //     let errorValidate = newDocument.validateSync();

    //     if( errorValidate ) {
    //         LogsTools.write({ element: `${this.name}: CREATE VALIDATE`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: errorValidate });
    //         return { error: errorValidate };
    //     }
        
    //     try {
    //         const savedDocument = await this.model.create(newDocument);
    //         const id = savedDocument._id.toString();
            
    //         LogsTools.write({ element: `${this.name}: CREATE - ${id}`, textColour: LogsTools.TextColours.Green });
    //         LogsTools.write({ element: inputs });

    //         return { id: id };
    //     } catch(errorInsert) {
    //         LogsTools.write({ element: `${this.name}: CREATE - ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: errorInsert });
    //         return { error: errorInsert };
    //     }
    // }

    static async read({ id }) {
        try {
            let row = (await this.model.findById(id).exec()).toObject({ versionKey: false });
            MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });

            LogsTools.write({ element: `${this.name}: READ - ${id}`, textColour: LogsTools.TextColours.Green });

            return row;
        } catch(errorRead) {
            LogsTools.write({ element: `${this.name}: READ ERROR - ${id}`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: errorRead });
            return { error: errorRead };
        }
    }

    // static async update({ id, inputs }) {
    //     try {
    //         inputs.updatedDate = new Date();

    //         delete inputs.insertedDate;
    //         delete inputs._id;

    //         const opts = { runValidators: true };
    //         const response =  await this.model.findByIdAndUpdate( id, inputs, opts);

    //         LogsTools.write({ element: `${this.name}: UPDATE - ${id}`, textColour: LogsTools.TextColours.Green });
    //         LogsTools.write({ element: inputs });

    //         return { id: response._id.toString(), updatedDate: inputs.updatedDate };
    //     } catch(errorUpdate) {
    //         LogsTools.write({ element: `${this.name}: UPDATE ERROR - ${id}`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: inputs });
    //         LogsTools.write({ element: errorUpdate });
    //         return { error: errorUpdate };
    //     }
    // }

    // static async delete({ id }) {
    //     try {
    //         const response =  await this.model.findByIdAndDelete( id );

    //         LogsTools.write({ element: `${this.name}: DELETE - ${id}`, textColour: LogsTools.TextColours.Green });

    //         return { id: response._id.toString(), deletedDate: new Date() };
    //     } catch(errorDelete) {
    //         LogsTools.write({ element: `${this.name}: DELETE ERROR - ${id}`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: errorDelete });
    //         return { error: errorDelete };
    //     }
    // }

    // static async deleteMany({ inputs }) {
    //     try {
    //         const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
    //         let deleteFilters = {};

    //         LogsTools.write({ element: `${this.name}: DELETEMANY`, textColour: LogsTools.TextColours.Yellow });            
    //         if( filters['$and'] && Object.keys(filters['$and']).length > 0) {               
    //             for( const filter of filters['$and']) {
    //                 for( const[key, value] of Object.entries(filter) ) {
    //                     deleteFilters[key] = value["$eq"];
    //                 }
    //             }
    //         }
    //         LogsTools.write({ element: deleteFilters });

    //         let feedback = { acknowledged: false };
    //         if( Object.keys(deleteFilters).length > 0) {
    //             feedback = await this.model.deleteMany(deleteFilters);
    //             // feedback { acknowledged: true, deletedCount: 1 }
    
    //             LogsTools.write({ element: `${this.name}: DELETEMANY - feedback: ${feedback.acknowledged}, deletedCount: ${feedback.deletedCount}`, textColour: LogsTools.TextColours.Yellow });
    //         } else {
    //             LogsTools.write({ element: `${this.name}: DELETEMANY ERROR - no filters`, textColour: LogsTools.TextColours.Red });
    //         }

    //         return { feedback: feedback, deletedDate: new Date() };
    //     } catch(errorDelete) {
    //         LogsTools.write({ element: `${this.name}: DELETE ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: errorDelete });
    //         return { error: errorDelete };
    //     }
    // }
}