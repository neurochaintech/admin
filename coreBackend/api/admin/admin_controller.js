
"use strict";

import BlocksDAO from "../blocks/blocks_dao.js"
import TransactionsDAO from "../transactions/transactions_dao.js"

import MongodbODM from "../mongodb_odm.js";

export default class AdminController {

    static async list(req, res, next) {
        if( req.query.publicKey && req.query.blockNumber_from && req.query.blockNumber_to  ) {
            let output = [];

            // Get blocks
            
            const blocksRows = await BlocksDAO.list({ inputs: {
                "and_branchPath.blockNumber_gte": req.query.blockNumber_from,
                "and_branchPath.blockNumber_lt": req.query.blockNumber_to,
                "and_branch_eq": "MAIN",
                "sort": "branchPath.blockNumber_-1",
                "retrievedFields": "block.header.id.data block.header.timestamp.data block.header.previousBlockHash.data branchPath.blockNumber",
                elementsPerPage: Number.MAX_SAFE_INTEGER,
                page: 0
            } , sanitise: false });

            let blocks = {};
            let blocksIds = [];
            for(let iRow = 0; iRow < blocksRows.length; ++iRow) {
                let row = blocksRows[iRow].toObject({ versionKey: false });
                MongodbODM.sanitiseOutput({ row: row, modelFields: BlocksDAO.model.schema.paths });
                blocks[row.block.header.id.data] = row;
                blocksIds.push(row.block.header.id.data);
            }

            // console.log("blocksIds.length", blocksIds.length);

            let transactionsRows;
            try {
                transactionsRows = await TransactionsDAO.model.find({
                    '$and': [
                        {
                            'blockId.data': {
                                '$in': blocksIds
                            }
                        },
                        {
                            '$or': [
                                { 'transaction.inputs.keyPub.rawData': { '$eq': req.query.publicKey } },
                                { 'transaction.outputs.keyPub.rawData': { '$eq': req.query.publicKey } }
                            ]
                        }
                    ]
                })
                .limit(Number.MAX_SAFE_INTEGER)
                .skip(0)
                .exec();
            } catch(errorList) {
                LogsTools.write({ element: errorList });
            }

            let blocksIdsIndexes = {};
            for(let iRow = 0; iRow < transactionsRows.length; ++iRow) {
                let row = transactionsRows[iRow].toObject({ versionKey: false });
                MongodbODM.sanitiseOutput({ row: row, modelFields: TransactionsDAO.model.schema.paths });
                
                const blockId = row.blockId.data;
                let index = -1;
                if(!(blockId in blocksIdsIndexes)) {
                    index = output.length;
                    blocksIdsIndexes[blockId] = index;

                    output[index] = blocks[blockId];
                    output[index]["transactions"] = [];
                }
                index = blocksIdsIndexes[blockId];
                output[index]["transactions"].push(row);
            }

            res.status(200).json({ count: output.length, rows: output });
        } else {
            res.status(400).json({ message: "Missing publicKey or blockNumber_from or blockNumber_to" });
        }

        next();
    }
}