
"use strict";

import { Router } from "express";

import AdminController from "./admin_controller.js";

const router = new Router();

router
    .route("/admin/transactions")
    .get( (req, res, next ) => AdminController.list(req, res, next) )
;

export default router;