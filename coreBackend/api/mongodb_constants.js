export default class MongodbConstants {
    static ActiveEnum = {
        Active: "active",
        Archived: "archived",
        Deleted: "deleted",
    };
}