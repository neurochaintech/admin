"use strict";

// import LogsTools from "../tools/logs_tools.js";

export default class Controller {
    static dao;
    
    static async list(req, res, next) {
        const rows = await this.dao.list({ inputs: req.query });

        if( rows ) {
            res.status(200).json({ rows: rows });
        } else {
            res.status(400).json(rows);
        }

        next();
    }


    // static async create(req, res, next) {
    //     const feedback = await this.dao.create({ inputs: req.body });

    //     if( feedback.id ) {
    //         res.status(201).json(feedback);
    //     } else {
    //         res.status(400).json(feedback);
    //     }

    //     next();
    // }

    static async read(req, res, next) {
        const row = await this.dao.read({ id: req.params.id });

        if( row.id ) {
            res.status(200).json({ row: row });
        } else {
            res.status(400).json(row);
        }

        next();
    }

    // static async update(req, res, next) {
    //     const feedback = await this.dao.update({ id: req.params.id, inputs: req.body });

    //     if( feedback.id ) {
    //         res.status(200).json(feedback);
    //     } else {
    //         res.status(400).json(feedback);
    //     }

    //     next();
    // }

    // static async delete(req, res, next) {
    //     const response = await this.dao.delete({ id: req.params.id });

    //     if( response.id ) {
    //         res.status(200).json(response);
    //     } else {
    //         res.status(400).json(response);
    //     }

    //     next();
    // }

    // static async deleteMany(req, res, next) {
    //     const response = await this.dao.deleteMany({ inputs: req.body });

    //     if( response.feedback && response.feedback.acknowledged ) {
    //         res.status(200).json(response);
    //     } else {
    //         res.status(400).json(response);
    //     }

    //     next();
    // }
}