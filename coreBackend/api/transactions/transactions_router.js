
"use strict";

import { Router } from "express";

import TransactionsController from "./transactions_controller.js";

const router = new Router();

router
    .route("/transactions")
    .get( (req, res, next ) => TransactionsController.list(req, res, next) )
;

router
    .route("/transactions/:id")
    .get( (req, res, next ) => TransactionsController.read(req, res, next) )
;

export default router;