
"use strict";

// import mongoose from "mongoose";
import LogsTools from "../../tools/logs_tools.js";
import MongodbODM from "../mongodb_odm.js";

// import MongodbConstants from "../mongodb_constants.js";
import MongodbDAO from "../mongodb_dao.js";

export default class TransactionsDAO extends MongodbDAO {
    static COLLECTION_NAME = "transactions";
    static SCHEMA = {
        "transaction.inputs.keyPub.rawData": { type: String },
        "transaction.inputs.value.value": { type: String },
        "transaction.outputs.keyPub.rawData": { type: String },
        "transaction.outputs.value.value": { type: String },
        "transaction.lastSeenBlockId.data": { type: String },
        "transaction.fees.value": { type: Number },
        "blockId.data": { type: String },
        "timestamp.data": { type: Number },
    };
    
    // static async list({ inputs }) {
    //     let list = [];

    //     const elementsPerPage = inputs.elementsPerPage ? parseInt(inputs.elementsPerPage) : 20;
    //     const page = inputs.page ? parseInt(inputs.page) : 0;
    //     const skip = inputs.from ? inputs.from : elementsPerPage * page;
    //     const limit = inputs.to ? inputs.from - inputs.to : elementsPerPage;

    //     const filters = MongodbODM.formatFilters({ parameters: inputs, modelFields: this.model.schema.paths });
    //     const sortOptions = MongodbODM.formatSort({ parameters: inputs, modelFields: this.model.schema.paths });
        
    //     const retrievedFields = inputs.retrievedFields ? inputs.retrievedFields : "-__v";

    //     LogsTools.write({ element: `${this.name}: LIST` });
    //     LogsTools.write({ element: filters });
    //     LogsTools.write({ element: sortOptions });
    //     LogsTools.write({ element: `elementsPerPage:${elementsPerPage} page:${page}` });

    //     try {
    //         if( inputs.count ) {
    //             const count = await this.model.countDocuments(filters);
    //             list.push({count: count});
    //         } else {
    //             // const listCursors = await this.model.find(filters, retrievedFields)
    //             //     .limit(limit)
    //             //     .skip(skip)
    //             //     .sort(sortOptions)
    //             //     .exec();
                
    //             // for(let iRow = 0; iRow < listCursors.length; ++iRow) {
    //             //     let row = listCursors[iRow].toObject({ versionKey: false });
    //             //     MongodbODM.sanitiseOutput({ row: row, modelFields: this.model.schema.paths });
    //             //     list.push(row);
    //             // }

    //             const listCursors = await this.model.aggregate([
    //                 { $match: filters }
    //             ])
    //             .limit(limit)
    //             .skip(skip)
    //             .sort(sortOptions)
    //             .lookup({ from: 'blocks', localField: 'blockId.data', foreignField: 'block.header.id.data', as: 'blocks' });
    //             list = listCursors;
    //         }
    //     } catch(errorList) {
    //         LogsTools.write({ element: `${this.name}: LIST ERROR`, textColour: LogsTools.TextColours.Red });
    //         LogsTools.write({ element: inputs });
    //         LogsTools.write({ element: errorList });
    //     }

    //     return list;
    // }
}