
"use strict";

import Controller from "../controller.js";

import TransactionsDAO from "./transactions_dao.js";

export default class TransactionsController extends Controller {
    static dao = TransactionsDAO;

    static async read(req, res, next) {
        const row = await this.dao.findOne({ inputs: {
            "transaction.id.data": req.params.id
        } });

        if( row.id ) {
            res.status(200).json({ row: row });
        } else {
            res.status(400).json(row);
        }

        next();
    }
}