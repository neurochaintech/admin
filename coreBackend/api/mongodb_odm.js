"use strict";

import mongoose from "mongoose";
import MongodbConstants from "./mongodb_constants.js";
import LogsTools from "../tools/logs_tools.js";

const LOGICAL_OPERATORS = [
    "and",
    "or",
];

const COMPARISON_OPERATORS_TEXT = [
    "like",
    "nlike",
    "eq",
    "ne",
    "in",
    "nin"
];

const COMPARISON_OPERATORS_NUMBERS = [
    "eq",
    "gt",
    "gte",
    "lt",
    "lte",
    "ne",
];

const COMPARISON_OPERATORS_BOOLEAN = [
    "eq"
];

const COMPARISON_OPERATORS_ID = [
    "eq",
    "ne",
    "in",
    "nin"
];

// YYYY-MM-DD
const DATE_PATTERN = /^\d{4}[./-]\d{2}[./-]\d{2}$/
// YYYY-MM-DDTHH:MM:SS
const DATE_WITH_TIME_PATTERN = /^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)[ \/T\/t]([01][0-9]|2[0-3]):([0-5][0-9])(?::([0-5][0-9]))?$/;

export default class MongodbODM {
    
    static sanitiseOutput({ row, modelFields }) {
        for (const [key, value] of Object.entries(row)) {
            if( modelFields[key]
                && modelFields[key].instance === "ObjectID" 
                && value ) {
                row[key] = value.toString();
            }
        }

        if(row._id) {
            row.id = row._id;
            delete row._id;
        }
    }

    static getQueryElements({ input, modelFields }) {
        let output = null;

        try {
            let subElements = input.split("_");
            const logicalOperator = subElements.shift();
            const comparisonOperator = subElements.pop();

            let field = subElements.join("_");
            if(field === "id") { field = "_id"; }

            if( LOGICAL_OPERATORS.includes(logicalOperator) && modelFields[field] ) {
                let valid = false;
                
                switch( modelFields[field].instance ) {
                    case "String":
                        valid = COMPARISON_OPERATORS_TEXT.includes(comparisonOperator);
                        break;
                    case "Number":
                    case "Date":
                        valid = COMPARISON_OPERATORS_NUMBERS.includes(comparisonOperator);
                        break;
                    case "Boolean":
                        valid = COMPARISON_OPERATORS_BOOLEAN.includes(comparisonOperator);
                        break;
                    case "ObjectID":
                        valid = COMPARISON_OPERATORS_ID.includes(comparisonOperator);
                        break;
                    default:
                        LogsTools.write({ element: `${this.name} NOT FOUND: getQueryElements ${field}`, textColour: LogsTools.TextColours.Yellow });
                        break;
                }
                
                output = {
                    logicalOperator: logicalOperator,
                    comparisonOperator: comparisonOperator,
                    field: field,
                    valid: valid,
                }
            } else {
                // LogsTools.write({ element: `${this.name} ERROR: getQueryElements - not found "${logicalOperator}" or "${field}"`, textColour: LogsTools.TextColours.Red });
            }
        } catch(error) {
            LogsTools.write({ element: `${this.name} ERROR: getQueryElements`, textColour: LogsTools.TextColours.Red });
            LogsTools.write({ element: input });
            LogsTools.write({ element: error });
        }

        return output;
    }

    // Exemple : ?and_name_eq=toto
    // Exemple : ?or_score_gt=2
    static formatFilters({ parameters, modelFields }) {
        let filters = {
            "$and": [],
            "$or": []
        };

        for (const [key, value] of Object.entries(parameters)) {
            if(key === "sort") continue;

            const queryElements = this.getQueryElements({ input: key, modelFields: modelFields });

            // LogsTools.write({ element: `${this.name}: formatFilters inputs`, textColour: LogsTools.TextColours.Magenta });
            // LogsTools.write({ element: key, textColour: LogsTools.TextColours.Magenta });
            // LogsTools.write({ element: value, textColour: LogsTools.TextColours.Magenta });

            if( queryElements && queryElements.valid ) {

                // LogsTools.write({ element: queryElements, textColour: LogsTools.TextColours.Magenta });
                // LogsTools.write({ element: modelFields[queryElements.field].instance, textColour: LogsTools.TextColours.Magenta });

                const filerLogical = `$${queryElements.logicalOperator}`
                let filter = {};
                switch(modelFields[queryElements.field].instance) {
                    case "String":
                        if( queryElements.comparisonOperator === "like" ) {
                            filter[queryElements.field] = { "$regex": new RegExp( value, "i") };
                        } else if( queryElements.comparisonOperator === "nlike" ) {
                            filter[queryElements.field] = { "$regex": new RegExp( `?!${value}`, "i") };
                        } else {
                            let operation = {};

                            let parsed = value;
                            if( queryElements.comparisonOperator === "in" || queryElements.comparisonOperator === "nin" ) {
                                parsed = value.split(",");
                            }
                            operation[`$${queryElements.comparisonOperator}`] = parsed;
                            
                            filter[queryElements.field] = operation;
                        }
                        break;

                    case "Number":
                        let operationNumber = {};
                        operationNumber[`$${queryElements.comparisonOperator}`] = parseFloat(value);
                        filter[queryElements.field] = operationNumber;
                        break;

                    case "Boolean":
                        if( queryElements.comparisonOperator === "eq" ) {
                            const parsed = value === true || (value === "true");
                            filter[queryElements.field] = parsed;
                        }
                        break;

                    case "ObjectID":
                       
                            let operation = {};

                            if( queryElements.comparisonOperator === "eq" || queryElements.comparisonOperator === "ne" ) {
                                try {
                                    const parsed = new mongoose.Types.ObjectId(value);
                                    operation[`$${queryElements.comparisonOperator}`] = parsed;
                                    filter[queryElements.field] = operation;
                                } catch(e) {
                                    LogsTools.write({ element: `${this.name}: Unable to convert ${value} ObjectId`, textColour: LogsTools.TextColours.Red });
                                }
                            } else if( queryElements.comparisonOperator === "in" || queryElements.comparisonOperator === "nin" ) {
                                const idsStrings = value.split(",");

                                let parsed = [];
                                for( const idString of idsStrings ) {
                                    try {
                                        parsed.push( new mongoose.Types.ObjectId(idString) )
                                    } catch(e) {
                                        LogsTools.write({ element: `${this.name}: Unable to convert ${idString} ObjectId`, textColour: LogsTools.TextColours.Red });
                                    }
                                }
                                operation[`$${queryElements.comparisonOperator}`] = parsed;
                            
                                filter[queryElements.field] = operation;
                            }
                        break;
                    
                        case "Date":
                            if( value.match(DATE_PATTERN) || value.match(DATE_WITH_TIME_PATTERN) ) {
                                let operationDate = {};
                                operationDate[`$${queryElements.comparisonOperator}`] = new Date(value);
                                filter[queryElements.field] = operationDate;
                            } else if( value === "null" ) {
                                filter[queryElements.field] = null;
                            }
                            break;

                    default:
                        LogsTools.write({ element: `${this.name} NOT FOUND: formatFilters ${modelFields[key].instance}`, textColour: LogsTools.TextColours.Yellow });
                        break;
                }
                filters[filerLogical].push( filter );

                // LogsTools.write({ element: filter, textColour: LogsTools.TextColours.Magenta });
            }
        }

        if( typeof filters["$and"]["active"] === undefined ) {
            !filters["$and"].push({ "active": MongodbConstants.ActiveEnum.Active });
        }

        if (filters["$and"].length === 0) { 
            delete filters["$and"];
        }
        if (filters["$or"].length === 0) { 
            delete filters["$or"];
        }

        // LogsTools.write({ element: filters });

        return filters;
    }

    // Exemple : &sort=number_-1,name_1
    static formatSort({ parameters, modelFields }) {
        let sortOptions = {};

        if( parameters["sort"] ) {
            const rawValues = parameters["sort"];
            const sortElements = rawValues.split(",");
            for( const sortElement of sortElements ) {
                let subElements = sortElement.split("_");
                const order = subElements.pop();
                const field = subElements.join("_");

                if( modelFields[field] ) {
                    sortOptions[field] = order;
                }
            }
        }

        if(Object.keys(sortOptions).length === 0) {
            sortOptions = { "insertedDate": -1 };
        }

        return sortOptions;
    }
}
