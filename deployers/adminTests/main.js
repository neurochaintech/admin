"use strict";

import axios from 'axios';

const HOST6 = "ec2-13-39-76-45.eu-west-3.compute.amazonaws.com";
const HOST1 = "ec2-13-38-38-5.eu-west-3.compute.amazonaws.com";

function getUrlRadix({ networkId, nodeId }) {
    const REACT_APP_BACKEND_URI = `http://${HOST6}:5821/api`;
    return `${REACT_APP_BACKEND_URI}/networks/${networkId}/nodes/${nodeId}/transactions`;
}

async function send({ networkId, nodeId, transaction }) {
    let url = `${getUrlRadix({ networkId: networkId, nodeId: nodeId })}`;
    
    console.log("Transaction", transaction);

    try {
        const response = await axios.post(url, transaction);
        console.log(response.status);
        console.log(response.data.row);
    } catch (e) {
        console.log("ERROR", e.response.data);
    }
}

async function getBalance({ publicKey }) {
    const urlAPI = `http://${HOST1}:8080`;
    const url = `${urlAPI}/balance`;
    const data = {
        rawData: publicKey
    };        

    let output = { status: 400, data: {} };
    try {
        const response = await axios.post(url, data);
        output.status = response.status;
        output.data = response.data;
    } catch (e) {
        console.log("ERROR", e.response.data);
    }

    return output;
}

async function main() {
    // const messages = [
    //     "à, è, ì, ò, ù, À, È, Ì, Ò, Ù",
    //     "á, é, í, ó, ú, ý, Á, É, Í, Ó, Ú, Ý",
    //     "â, ê, î, ô, û, Â, Ê, Î, Ô, Û",
    //     "ã, ñ, õ, Ã, Ñ, Õ",
    //     "ä, ë, ï, ö, ü, ÿ, Ä, Ë, Ï, Ö, Ü, Ÿ",
    //     "å, Å",
    //     "æ, Æ",
    //     "œ, Œ",
    //     "ç, Ç",
    //     "ð, Ð",
    //     "ø, Ø",
    //     "¿",
    //     "¡",
    //     "ß",
    // ];

    // for( const message of messages ) {
    //     const transaction = {
    //         authorPublicKey: "AxzvUGGzye5oFb7Kcgla96fvAp5FlRPl89RxUnrQDj3a",
    //         authorPrivateKey: "246a088fc1f32513b4afb0a1d7a1c24e2f80ea6dc7b2f1423e45a1916a075cbb",
    //         recipientPublicKey: "AkLBRdrcT8HSCTr48yhAxiDaSE7DKWeGI0UWeDEFniGo",
    //         value: 1,
    //         data: message,
    //         fee: 0,
    //     };
        
    //     await send({ 
    //         networkId: "testnet6", 
    //         nodeId: "Bot-testnet6-5",
    //         transaction: transaction 
    //     });
    // }

    // Show balances
    console.log( "Node", (await getBalance({ publicKey: "AxzvUGGzye5oFb7Kcgla96fvAp5FlRPl89RxUnrQDj3a" })) );
    console.log( "Wallet", (await getBalance({ publicKey: "AkMp0AYrgJfTRQmA6E/gXTRQ3jSu1f3CboSWHuLaq1Sm" })) );

    const now = new Date();

    const txFromNodeToWallet = {
        authorPublicKey: "AxzvUGGzye5oFb7Kcgla96fvAp5FlRPl89RxUnrQDj3a",
        authorPrivateKey: "246a088fc1f32513b4afb0a1d7a1c24e2f80ea6dc7b2f1423e45a1916a075cbb",
        recipientPublicKey: "AkMp0AYrgJfTRQmA6E/gXTRQ3jSu1f3CboSWHuLaq1Sm",
        value: 100000,
        data: `TEST node -> wallet ${now}`,
        fee: 0,
    };
    await send({ 
        networkId: "testnet6", 
        nodeId: "Bot-testnet6-5",
        transaction: txFromNodeToWallet 
    });

    // New wallet to node
    const txFromWalletToNode = {
        authorPublicKey: "AkMp0AYrgJfTRQmA6E/gXTRQ3jSu1f3CboSWHuLaq1Sm",
        authorPrivateKey: "bcec16c09bcebc17b82b2ffea57cbef66ccc64042b139f9a20a1f700b19cd924",
        recipientPublicKey: "AxzvUGGzye5oFb7Kcgla96fvAp5FlRPl89RxUnrQDj3a",
        value: 1,
        data: `TEST wallet -> node ${now}`,
        fee: 0,
    };
    await send({ 
        networkId: "testnet6", 
        nodeId: "Bot-testnet6-5",
        transaction: txFromWalletToNode 
    });
};
main();