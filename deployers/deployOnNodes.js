"use strict";

import SshTools from "./tools/ssh_tools.js";
import FileSystemTools from "./tools/fileSystem_tools.js";

const networkPath = `${process.cwd()}/../backend/inputs/networks/testnet6`;

async function main() {
    try {
        console.log("Deploy on Nodes");

        const sshPath = `${networkPath}/sshKey.pem`;
        const configPath = `${networkPath}/config.json`;
        const outputDirPath = `~/admin/${process.argv[2]}`;

        console.log("Output: ", outputDirPath);

        const config = FileSystemTools.readJson({ inputPath: configPath });
        for( const [key, value] of Object.entries(config) ) {
            // Deply only on 1 node
            if(key !== "Bot-testnet6-6") continue;

            console.log("\n\nNodes:", key);
            console.log(`ssh -i "${sshPath}" ${value.username}@${value.hostname}`);

            console.log("\nPrepare output", outputDirPath);
            await SshTools.send({ 
                hostname: value.hostname, 
                username: value.username, 
                sshKeyPath: sshPath,
                command: `rm -rf ${outputDirPath}`
            });
            await SshTools.send({ 
                hostname: value.hostname, 
                username: value.username, 
                sshKeyPath: sshPath,
                command: `mkdir -p ${outputDirPath}`
            });
            
            console.log("\nCopy files");
            const pathToScan = `${process.cwd()}/../${process.argv[2]}`
            const inputFilesPaths = FileSystemTools.getFilesPaths({ inputPath: pathToScan, recursive: false });
            for( const inputPath of inputFilesPaths ) {
                const pathRadix = inputPath.replaceAll(process.cwd(), "");

                if( !pathRadix.includes("node_modules") ) {
                    const outputPath = `${outputDirPath}${pathRadix}`;
                    console.log(inputPath, "->" ,outputPath);
                    await SshTools.copyTo({
                        hostname: value.hostname, 
                        username: value.username, 
                        sshKeyPath: sshPath,
                        remotePath: outputPath,
                        localPath:  inputPath,
                        timeout: 5 * 1000 
                    });
                }
            }

            console.log("Don't forget to change the URL in .env.production or to npm install --force.");

            // console.log(`\nDeploying docker`);
            // await SshTools.send({ 
            //     hostname: value.hostname, 
            //     username: value.username, 
            //     sshKeyPath: sshPath,
            //     command: `cd ${outputDirPath} && ./deploy_docker.sh`,
            //     timeout: 5 * 1000 
            // });
            // const logContainerList = await SshTools.send({ 
            //     hostname: value.hostname, 
            //     username: value.username, 
            //     sshKeyPath: sshPath,
            //     command: `docker container list --all`,
            //     timeout: 5 * 1000 
            // });
            // console.log(logContainerList);
        }
        process.exit(0);

    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};
main();