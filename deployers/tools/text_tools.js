"use strict";

export default class TextTools {

    static substringsInString({ string, array }){
        return array.some(substring => {
            if (string.includes(substring)) {
              return true;
            }
          
            return false;
        });
    }

}