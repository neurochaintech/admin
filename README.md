# Admin

## Prerequisites

```
cd backend
npm install

cd frontend
npm install
```

## Launch

```
./start-dev.sh
```

```
cd backend
npm run start-dev

cd frontend
npm run start-dev
```

## VERY IMPORTANT

### I can't send transaction

You must use the private key's exponent to send transaction!

## Notes

```
http://ec2-13-39-76-45.eu-west-3.compute.amazonaws.com:5820
http://ec2-13-39-76-45.eu-west-3.compute.amazonaws.com:5821
```